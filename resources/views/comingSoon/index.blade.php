<!DOCTYPE html>
<html lang="en">
<head>
	<title>Smart-Unir | Coming Soon</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="{{ asset('home_page/images/logo_unir.jpg') }}"/>

	<link rel="stylesheet" type="text/css" href="{{ asset('coming_soon/css/select2.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('coming_soon/css/util.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('coming_soon/css/main.css') }}">

    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>

	<!--  -->
	<div class="simpleslide100">
		<div class="simpleslide100-item bg-img1" style="background-image: url({{ asset('coming_soon/images/bg01.jpg') }});"></div>
		<div class="simpleslide100-item bg-img1" style="background-image: url({{ asset('coming_soon/images/bg02.jpg') }});"></div>
		<div class="simpleslide100-item bg-img1" style="background-image: url({{ asset('coming_soon/images/bg01.jpg') }});"></div>
	</div>

	<div class="size1 overlay1">
		<!--  -->
		<div class="size1 flex-col-c-m p-l-15 p-r-15 p-t-50 p-b-50">
			<h3 class="l1-txt1 txt-center p-b-25">
                Bientôt disponible
			</h3>

			<p class="m2-txt1 txt-center p-b-48">
				{{__('Our site is not yet launched. Stay turned by subscribing to our newsletter!')}}
			</p>

			<div class="flex-w flex-c-m cd100 p-b-33">
				<div class="flex-col-c-m size2 bor1 m-l-15 m-r-15 m-b-20">
					<span class="l2-txt1 p-b-9 days">35</span>
					<span class="s2-txt1">{{__('Days')}}</span>
				</div>

				<div class="flex-col-c-m size2 bor1 m-l-15 m-r-15 m-b-20">
					<span class="l2-txt1 p-b-9 hours">17</span>
					<span class="s2-txt1">{{__('Hours')}}</span>
				</div>

				<div class="flex-col-c-m size2 bor1 m-l-15 m-r-15 m-b-20">
					<span class="l2-txt1 p-b-9 minutes">50</span>
					<span class="s2-txt1">{{__('Minutes')}}</span>
				</div>

				<div class="flex-col-c-m size2 bor1 m-l-15 m-r-15 m-b-20">
					<span class="l2-txt1 p-b-9 seconds">39</span>
					<span class="s2-txt1">{{__('Seconds')}}</span>
				</div>
			</div>

			<form class="w-full flex-w flex-c-m validate-form" action="{{ route('store.newsletter') }}" method="get">
				<div class="wrap-input100 where1">
					<input class="input100 s2-txt2 form-control text-center {{$errors->has('Email')?'is-invalid':'' }}"
                           type="text" name="Email" placeholder="{{__('Enter Email Address')}}">
                    <input type="hidden" name="coming-soon" value="coming-soon">
                    @if ($errors->has('Email'))
                        <span class="invalid-feedback" style="color: red">
                            <strong>{{ $errors->first('Email') }}</strong>
                        </span>
                    @endif
				</div>
				<button class="size3 s2-txt3 how-btn1 where1" type="submit">
					Souscrire
				</button>
			</form>
		</div>
	</div>

    <script src="{{ asset('template/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('template/js/popper.min.js') }}"></script>
    <script src="{{ asset('template/js/bootstrap.min.js') }}"></script>

	<script src="{{ asset('coming_soon/js/countdowntime.js') }}"></script>
	<script>
        var coming_soon_date = new Date();

        $('.cd100').countdown100({
			endtimeYear: 0,
			endtimeMonth: 0,
			endtimeDate: parseInt("{{ $remaining_days }}") - 1,
			endtimeHours: 24 - coming_soon_date.getHours(),
			endtimeMinutes: 60 - coming_soon_date.getMinutes(),
			endtimeSeconds: 60 - coming_soon_date.getSeconds(),
			timeZone: ""
		});
	</script>
	<script src="{{ asset('coming_soon/js/main.js') }}"></script>
    @include('flashy::message')
</body>
</html>
