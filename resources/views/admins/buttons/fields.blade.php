<!-- Label Field -->
<div class="form-group col-sm-6">
    <b>{!! Form::label('label', __('Label:')) !!}</b>
    {!! Form::text('label', null, ['class' => 'form-control']) !!}
</div>

<!-- Link Field -->
<div class="form-group col-sm-6">
    <b>{!! Form::label('link', __('Link:')) !!}</b>
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

<!-- -->
<div class="form-group col-sm-6">
    <b>{!! Form::label('color', __('Color:')) !!}</b>
    {!! Form::select('color', \App\Models\Button::COLORS,
        null, ['placeholder' => __('Select the color'),'class' => 'form-control'])
    !!}
</div>

<!-- Visibility Field -->
<div class="form-group col-sm-6">
    <b>{!! Form::label('visibility', __('Visibility:')) !!}</b>
    <label class="checkbox-inline">
        {!! Form::hidden('visibility', 0) !!}
        {!! Form::checkbox('visibility', '1', null) !!}
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('Save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('admin.buttons.index') }}" class="btn btn-secondary">{{__('Cancel')}}</a>
</div>

@section('styles')
    <link rel="stylesheet" href="{{ asset('template/css/selectize.bootstrap3.min.css') }}"/>
@endsection

<!-- Allows to search an option in select field -->
@section('scripts')
<script src="{{ asset('template/js/selectize.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $('select').selectize({
            sortField: 'text'
        });
    });
</script>
@endsection
