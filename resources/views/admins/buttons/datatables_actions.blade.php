{!! Form::open(['route' => ['admin.buttons.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    {{--<a href="{{ route('admin.buttons.show', $id) }}" class='btn btn-ghost-success' title="{{__('Show')}}">
       <i class="fa fa-eye"></i>
    </a>--}}
    <a href="{{ route('admin.buttons.edit', $id) }}" class='btn btn-ghost-info' title="{{__('Edit')}}">
       <i class="fa fa-edit"></i>
    </a>
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-ghost-danger',
        'title' => __('Delete'),
    ]) !!}
</div>
{!! Form::close() !!}

<script type='text/javascript'>
    $(document).on("submit", "form", function (e) {
        e.preventDefault();
        Swal.fire({
            title: "{{__('Are you sure?') }}",
            html: "{{__('You won\'t be able to revert this!') }}",
            width: 400,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#cbc7c1',
            confirmButtonText: "{{ __('Yes') }}",
            cancelButtonText: "{{ __('Cancel') }}",
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                e.currentTarget.submit();
            }
        });
    });
</script>
