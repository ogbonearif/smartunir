@extends('admins.layouts.app')

@section('title')
    {{__('Edit Button')}}
@endsection

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('admin.buttons.index') !!}">{{__('Buttons')}}</a>
          </li>
          <li class="breadcrumb-item active">{{__('Edit')}}</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>{{__('Edit Button')}}</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($button, ['route' => ['admin.buttons.update', $button->id], 'method' => 'patch']) !!}

                              @include('admins.buttons.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection
