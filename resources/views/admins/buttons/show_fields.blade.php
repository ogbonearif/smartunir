<!-- Label Field -->
<div class="form-group">
    {!! Form::label('label', 'Label:') !!}
    <p>{{ $button->label }}</p>
</div>

<!-- Link Field -->
<div class="form-group">
    {!! Form::label('link', 'Link:') !!}
    <p>{{ $button->link }}</p>
</div>

<!-- Visibility Field -->
<div class="form-group">
    {!! Form::label('visibility', 'Visibility:') !!}
    <p>{{ $button->visibility }}</p>
</div>

