@extends('admins.layouts.app')

@section('title')
    {{__('Parameters')}}
@endsection

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item title-blue"><h2><b>{{__('Parameters')}}</b></h2></li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                             <i class="fa fa-align-justify"></i>
                             {{__('Parameters')}}
                             <a class="pull-right btn btn-primary" href="{{ route('admin.parameters.create') }}">
                                 <i class="fa fa-plus fa-lg"></i> <b>{{__('Add')}}</b>
                             </a>
                         </div>
                         <div class="card-body">
                             @include('admins.parameters.table')
                              <div class="pull-right mr-3">

                              </div>
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection

