@if( \Illuminate\Support\Str::contains(request()->getUri(), 'edit'))
    <div class="form-group col-sm-6">
        <b>{!! Form::label('label', __('Label:')) !!}</b>
        {!! Form::text('label', null, ['class' => 'form-control', 'readonly']) !!}
    </div>
@else
    <div class="form-group col-sm-6">
        <b>{!! Form::label('label', __('Label:')) !!}</b>
        {!! Form::text('label', null, ['class' => 'form-control']) !!}
    </div>
@endif

<!-- Description Field -->
<div class="form-group col-sm-6 col-lg-6">
    <b>{!! Form::label('description', __('Description:')) !!}</b>
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Value Field -->
{{--<div class="form-group col-sm-6 col-lg-6">
    <b>{!! Form::label('value', __('Value:')) !!}</b>
    {!! Form::textarea('value', null, ['class' => 'form-control']) !!}
</div>--}}
<div class="form-group col-sm-6">
    <b>{!! Form::label('value', __('Value:')) !!}</b>
    {!! Form::text('value', null, ['class' => 'form-control', 'id' => 'value']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('Save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('admin.parameters.index') }}" class="btn btn-secondary">{{__('Cancel')}}</a>
</div>

@section('scripts')
<script>
    $('#value').datetimepicker({
        format: 'YYYY-MM-DD',
        useCurrent: true,
        icons: {
            up: "icon-arrow-up-circle icons font-2xl",
            down: "icon-arrow-down-circle icons font-2xl"
        },
        //sideBySide: true
    });
</script>
@endsection
