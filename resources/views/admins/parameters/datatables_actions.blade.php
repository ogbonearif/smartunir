{!! Form::open(['route' => ['admin.parameters.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{{ route('admin.parameters.show', $id) }}" class='btn btn-ghost-success'>
       <i class="fa fa-eye"></i>
    </a>
    <a href="{{ route('admin.parameters.edit', $id) }}" class='btn btn-ghost-info'>
       <i class="fa fa-edit"></i>
    </a>
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-ghost-danger',
    ]) !!}
</div>
{!! Form::close() !!}

<script type='text/javascript'>
    $(document).on("submit", "form", function (e) {
        e.preventDefault();
        Swal.fire({
            title: "{{__('Are you sure?') }}",
            html: "La suppression de ce paramètre pourrait impacter le site.",
            width: 400,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#cbc7c1',
            confirmButtonText: "{{ __('Yes') }}",
            cancelButtonText: "{{ __('Cancel') }}",
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                e.currentTarget.submit();
            }
        });
    });
</script>
