<!-- Label Field -->
<div class="form-group">
    {!! Form::label('label', __('Label:')) !!}
    <p>{{ $parameter->label }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', __('Description:')) !!}
    <p>{{ $parameter->description }}</p>
</div>

<!-- Value Field -->
<div class="form-group">
    {!! Form::label('value', __('Value:')) !!}
    <p>{{ $parameter->value }}</p>
</div>

