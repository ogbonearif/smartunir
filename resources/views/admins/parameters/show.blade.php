@extends('admins.layouts.app')

@section('title')
    {{__('Show')}} {{__('Parameter')}}
@endsection

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('admin.parameters.index') }}">{{__('Parameter')}}</a>
            </li>
            <li class="breadcrumb-item active">{{__('Detail')}}</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>{{__('Details')}}</strong>
                                  <a href="{{ route('admin.parameters.index') }}" class="btn btn-light">{{__('Back')}}</a>
                             </div>
                             <div class="card-body">
                                 @include('admins.parameters.show_fields')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection
