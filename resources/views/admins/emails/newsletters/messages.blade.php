@component('mail::message')

# {{ $subject }}

{!! $message !!}

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

{{__('Thanks')}},<br>
{{ config('app.name') }}
@endcomponent
