@extends('admins.layouts.app')

@section('title')
   Edit Slide
@endsection

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('admin.slides.index') !!}">Slide</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Slide</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($slide, ['route' => ['admin.slides.update', $slide->id],  'enctype' => 'multipart/form-data',
                                    'method' => 'patch']) !!}

                              <!-- Image Field -->
                                  <div class="form-group">
                                      <a data-toggle="dropdown" href="#">
                                          <img class="img-fluid width: 100%" src="{{ $slide->image_url }}" alt="" width="570"
                                               height="300" style="margin-bottom: 20px"/>
                                      </a>
                                  </div>
                                <p style="color: #004cd6">Servez vous du champs <b>Image</b> pour changer l'image </p>
                                  @include('admins.slides.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection
