<!-- Image Field -->
<div class="form-group">
   {{-- {!! Form::label('image', 'Image:') !!}
    <p>{{ $slide->image }}</p>--}}
    <a data-toggle="dropdown" href="#">
        <img class="img-fluid width: 100%" src="{{ $slide->image_url }}" alt="" width="570"
             height="300" style="margin-bottom: 20px"/>
    </a>
</div>

<!-- Title1 Field -->
<div class="form-group">
    {!! Form::label('title1', 'Title1:') !!}
    <p>{{ $slide->title1 }}</p>
</div>

<!-- Title2 Field -->
<div class="form-group">
    {!! Form::label('title2', 'Title2:') !!}
    <p>{{ $slide->title2 }}</p>
</div>

<!-- Button Label Field -->
<div class="form-group">
    {!! Form::label('button_label', 'Button Label:') !!}
    <p>{{ $slide->button_label }}</p>
</div>

<!-- Button Link Field -->
<div class="form-group">
    {!! Form::label('button_link', 'Button Link:') !!}
    <p>{{ $slide->button_link }}</p>
</div>

<!-- Image Position Field -->
<div class="form-group">
    {!! Form::label('button_color', 'Button Color:') !!}
    <p>{{ $slide->button_color }}</p>
</div>

<!-- Image Position Field -->
<div class="form-group">
    {!! Form::label('image_position', 'Image Position:') !!}
    <p>{{ $slide->image_position }}</p>
</div>

