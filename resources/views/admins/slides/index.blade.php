@extends('admins.layouts.app')

@section('title')
    Slides
@endsection

@section('style')
{{--    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.css"/>--}}
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css"/>
@endsection

{{--@section('script')
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $("#sortable").sortable();
            $("#sortable").disableSelection();
        });
    </script>
@endsection--}}

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item title-blue"><h2><b>{{__('Liste des slides')}}</b></h2></li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            @include('flash::message')
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="search">
                            <div class="card-header">
                                <i class="fa fa-align-justify"></i>
                                Slides
                                <a class="pull-right btn btn-primary" href="{{ route('admin.slides.create') }}"><i
                                        class="fa fa-plus fa-lg"></i> <b>{{__('Add')}}</b></a>
                            </div>

                            <div class="card-body table-responsive">
                                <table id="table" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>@lang('Image')</th>
                                        <th>@lang('Title 1')</th>
                                        <th>@lang('Title 2')</th>
                                        <th>@lang('Button label')</th>
                                        <th>@lang('Button link')</th>
                                        <th>@lang('Button color')</th>
                                        <th>@lang('Actions')</th>
                                    </tr>
                                    </thead>
                                    <tbody id="table_contents">
                                    @foreach($slides as $slide)
                                        <tr class="row1" data-id="{{ $slide->id }}">
                                            <td>
                                                <img src="{{ $slide->image_url }}" alt=""
                                                     style="width: 100px; height: 50px;"/>
                                            </td>
                                            <td>{{ $slide->title1 }}</td>
                                            <td>{{ $slide->title2 }}</td>
                                            <td>{{ $slide->button_label }}</td>
                                            <td>{{ $slide->button_link }}</td>
                                            <td>
                                                {{ data_get(\App\Models\Button::COLORS, $slide->button_color) }}
                                            </td>
                                            <td>
                                                <div>
                                                    {!! Form::open(['route' => ['admin.slides.destroy', $slide->id], 'method' => 'delete']) !!}
                                                    <div class='btn-group'>
                                                        <a title="Voir" href="{{ route('admin.slides.show', $slide->id) }}" class='btn btn-ghost-success'>
                                                            <i class="fa fa-eye"></i>
                                                        </a>
                                                        <a title="Edit" href="{{ route('admin.slides.edit', $slide->id) }}" class='btn btn-ghost-info'>
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        {!! Form::button('<i class="fa fa-trash"></i>', [
                                                            'type' => 'submit',
                                                            'title' => 'Supprimer',
                                                            'class' => 'btn btn-ghost-danger',
                                                            'id' => 'delete_btn',
                                                            //'onclick' => "return confirm('Are you sure?')"
                                                        ]) !!}
                                                    </div>
                                                    {!! Form::close() !!}
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tr>
                                    </tr>

                                </table>
                                <div class="pull-right mr-3">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <!-- jQuery UI -->
    <script type="text/javascript" src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Datatables Js-->
    <script type="text/javascript" src="//cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script>

    <script>
        $('#table').DataTable({
            "language" : {
                "url" : "{{ asset('lang/' . config('app.locale') . '.json') }}"
            }
        });
    </script>

    <script type="text/javascript">
        $(function () {
            $("#table").DataTable();

            $("#table_contents").sortable({
                items: "tr",
                cursor: 'move',
                opacity: 0.6,
                update: function () {
                    sendOrderToServer();
                }
            });

            function sendOrderToServer() {

                var positions = [];
                $('tr.row1').each(function (index, element) {
                    positions.push({
                        id: $(this).attr('data-id'),
                        position: index + 1
                    });
                });

                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "{{ url('admin/slide/position/update') }}",
                    data: {
                        positions: positions,
                        _token: '{{csrf_token()}}'
                    },
                });
            }
        });
    </script>

    <script type='text/javascript'>
        $(document).on("submit", "form", function (e) {
            e.preventDefault();
            Swal.fire({
                title: "{{__('Are you sure?') }}",
                html: "{{__('You won\'t be able to revert this!') }}",
                width: 400,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#cbc7c1',
                confirmButtonText: "{{ __('Yes') }}",
                cancelButtonText: "{{ __('Cancel') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    e.currentTarget.submit();
                }
            });
        });
    </script>
@endsection
