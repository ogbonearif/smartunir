<!-- Image Field -->
<div class="form-group col-sm-6">
    <b>{!! Form::label('image', __('Image:')) !!}</b>
    {!! Form::file('image', ['id' => 'image', 'class' => 'form-control', 'accept' => 'image/*']) !!}
</div>

<!-- Title1 Field -->
<div class="form-group col-sm-6">
    <b>{!! Form::label('title1', __('Title 1:')) !!}</b>
    {!! Form::text('title1', null, ['class' => 'form-control']) !!}
</div>

<!-- Title2 Field -->
<div class="form-group col-sm-6">
    <b>{!! Form::label('title2', __('Title 2:')) !!}</b>
    {!! Form::text('title2', null, ['class' => 'form-control']) !!}
</div>

<!-- Button Label Field -->
<div class="form-group col-sm-6">
    <b>{!! Form::label('button_label', __('Button label:')) !!}</b>
    {!! Form::text('button_label', null, ['class' => 'form-control']) !!}
</div>

<!-- Button Link Field -->
<div class="form-group col-sm-6">
    <b>{!! Form::label('button_link', __('Button link:')) !!}</b>
    {!! Form::text('button_link', null, ['class' => 'form-control']) !!}
</div>

<!-- -->
<div class="form-group col-sm-6">
    <b>{!! Form::label('color', __('Button color:')) !!}</b>
    {!! Form::select('button_color', \App\Models\Button::COLORS,
        null, ['placeholder' => __('Select the color'),'class' => 'form-control'])
    !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('Save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('admin.slides.index') }}" class="btn btn-secondary">{{__('Cancel')}}</a>
</div>
@section('styles')
    <link rel="stylesheet" href="{{ asset('template/css/selectize.bootstrap3.min.css') }}"/>
@endsection

<!-- Allows to search an option in select field -->
@section('scripts')
    <script src="{{ asset('template/js/selectize.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('select').selectize({
                sortField: 'text'
            });
        });
    </script>

   {{-- <script type="text/javascript">
        /** image constraints
         * */
        $(document).on('submit', 'form', function (e) {
            var uploaded_image = document.getElementById('image');
            var infos = "";
            var display = false;
            var width = "{{ $image_width }}";
            var height = "{{ $image_height }}";
            var max_image_size = "{{ $max_image_size }}";
            var size_not_allowed = false;

            if (uploaded_image.size > parseInt(max_image_size)) {
                infos += "L'image est trop lourde";
                display = true;
                size_not_allowed = true;

            }
            if (size_not_allowed){
                infos +=  "Vous ne pouvez charger qu'un maximum de 8 Mo";
            }
            if(uploaded_image.height < parseInt(height) || uploaded_image.width < parseInt(width) ){
                infos += "Les dimensions de l'image doivent etre : " + "<br>";
                infos +="hauteur >= "+ height+ "<br>";
                infos += "largeur >= : "+ width +"<br>";
                infos += "Si vous continuez votre SLIDE ne sera pas enregistré" +  "<br>";
                display = true;
            }

            if(display){
                e.preventDefault();

                Swal.fire({
                    type: 'warning',
                    title: "warning",
                    html: "<div style='font-size: 15px'>" + infos + "</div>",
                    width: 400,
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: "Continuer",
                    cancelButtonText: "Annuler",
                    reverseButtons: true
                }).then((result) => {
                    if (result.value) {
                        e.currentTarget.submit();
                    }
                });
            }
        });
    </script>--}}
@endsection

