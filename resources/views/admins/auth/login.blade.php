<!DOCTYPE html>
<html lang="en">
<head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Smart-Unir Admin | {{__('Login')}}</title>
    <meta name="description" content="CoreUI Template - InfyOm Laravel Generator">
    <meta name="keyword" content="CoreUI,Bootstrap,Admin,Template,InfyOm,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <!-- Bootstrap-->
    <link rel="stylesheet" href="{{ asset('template/css/bootstrap.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('template/css/coreui.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://unpkg.com/@coreui/icons/css/coreui-icons.min.css">
    <link href="{{ asset('template/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('template/css/simple-line-icons.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('template/css/flag-icon.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/main_admin.css') }}">
</head>
<body class="app flex-row align-items-center">
<div class="container">
    <div class="row justify-content-center">
        <div {{--class="col-md-5"--}} class="col-sm-12 col-md-9 col-lg-8 col-xl-4">
            <h1 class="title-blue" style="text-align: center; margin-bottom: 25px">Smart-Unir</h1>
            <div class="card-group">
                <div class="card{{-- p-4--}}">
                    <div class="card-body">
                        <img src="{{ asset('home_page/images/logo_unir.jpg') }}" class="rounded mx-auto d-block" alt="Logo Unir" width="90" height="90">
                        <hr>
                        <form method="post" action="{{ route('admin.login') }}">
                            {!! csrf_field() !!}
                            <h1 class="title-blue">{{__('Connexion')}}</h1>
                            <p class="text-muted">{{__('Sign In to your account')}}</p>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                      <i class="icon-user" style="color: #009ee2"></i>
                                    </span>
                                </div>
                                <input type="email" class="form-control {{ $errors->has('email')?'is-invalid':'' }}" name="email" value="{{ old('email') }}"
                                       placeholder="{{__('Email')}}">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="input-group mb-4">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                      <i class="icon-lock" style="color: #009ee2"></i>
                                    </span>
                                </div>
                                <input type="password" class="form-control {{ $errors->has('password')?'is-invalid':'' }}" placeholder="{{__('Password')}}" name="password">
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                       <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <button class="btn btn-primary px-4" type="submit">{{__('Login')}}</button>
                                </div>
                                {{--<div class="col-6 text-right">
                                    <a class="btn btn-link px-0" href="{{ route('admin.password.request') }}">
                                        {{__('Forgot password?')}}
                                    </a>
                                </div>--}}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- CoreUI and necessary plugins-->
<script src="{{ asset('template/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('template/js/popper.min.js') }}"></script>
<script src="{{ asset('template/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('template/js/coreui.min.js') }}"></script>
<script src="{{ asset('template/js/perfect-scrollbar.js') }}"></script>
</body>
</html>
