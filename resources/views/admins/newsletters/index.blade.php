@extends('admins.layouts.app')

@section('title')
    Newsletters
@endsection

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item title-blue"><h2><b>{{__('List of subscribed emails')}}</b></h2></li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                             <i class="fa fa-align-justify"></i>
                             Newsletters
                             <a class="pull-right btn btn-primary" href="{{ route('admin.newsletters.create') }}">
                                 <i class="fa fa-plus fa-lg"></i> <b>{{__('Add')}}</b></a>
                         </div>
                         <div class="card-body">
                             @include('admins.newsletters.table')
                              <div class="pull-right mr-3">

                              </div>
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection

