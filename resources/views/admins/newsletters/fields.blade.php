<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', __('Email address:')) !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('Save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('admin.newsletters.index') }}" class="btn btn-secondary">{{__('Cancel')}}</a>
</div>
