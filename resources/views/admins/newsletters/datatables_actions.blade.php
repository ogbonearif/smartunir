{!! Form::open(['route' => ['admin.newsletters.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    {{--<a href="{{ route('admin.newsletters.show', $id) }}" class='btn btn-ghost-success'>
       <i class="fa fa-eye"></i>
    </a>--}}
    <a href="{{ route('admin.newsletters.edit', $id) }}" class='btn btn-ghost-info' title="{{__('Edit')}}">
       <i class="fa fa-edit"></i>
    </a>
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'title' => __('Delete'),
        'class' => 'btn btn-ghost-danger',
        'onclick' => "return confirm('Are you sure?')"
    ]) !!}
</div>
{!! Form::close() !!}
