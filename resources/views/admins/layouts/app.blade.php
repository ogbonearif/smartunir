<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Smart-Unir Admin | @yield('title') </title>
    <link rel="icon" href="{{ asset("home_page/images/logo_unir.jpg")}}">

    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 4.1.1 -->
    <link rel="stylesheet" href="{{ asset('template/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('template/css/bootstrap-datetimepicker.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('template/css/coreui.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://unpkg.com/@coreui/icons/css/coreui-icons.min.css">
    <link href="{{ asset('template/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('template/css/simple-line-icons.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('template/css/flag-icon.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/css/datatables.min.css') }}"/>
    <link rel="stylesheet" href="{{asset('css/custom-data-table.css')}}">

    <link rel="stylesheet" href="{{ asset('css/main_admin.css') }}">

    <!-- SweetAlert -->
    <script src="{{ asset('template/js/sweetalert2@8.js') }}"></script>
    @yield('styles')
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
<header class="app-header navbar header-background">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#">
        <img class="navbar-brand-full" src="{{ asset("home_page/images/logo_unir.jpg")}}" width="60" height="50"
             alt="Smart-Unir Logo">
        <img class="navbar-brand-minimized" src="{{ asset("home_page/images/logo_unir.jpg")}}" width="60"
             height="50" alt="Smart-Unir Logo">
    </a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>

    <ul class="nav navbar-nav ml-auto">
        {{--<li class="nav-item d-md-down-none">
            <a class="nav-link" href="#">
                <i class="icon-bell"></i>
                <span class="badge badge-pill badge-danger">5</span>
            </a>
        </li>--}}
        <li class="nav-item dropdown">
            <a class="nav-link" style="margin-right: 10px" data-toggle="dropdown" href="#" role="button"
               aria-haspopup="true" aria-expanded="false">
               <strong>{!! Auth::user()->full_name !!}</strong>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-header text-center">
                    <strong>{{__('Account')}}</strong>
                </div>
                {{--<a class="dropdown-item" href="#">
                    <i class="fa fa-envelope-o"></i> Messages
                    <span class="badge badge-success">42</span>
                </a>
                <div class="dropdown-header text-center">
                    <strong>Settings</strong>
                </div>--}}
                <a class="dropdown-item" href="#">
                    <i class="fa fa-user"></i>{{__('Profile')}}</a>
                <a class="dropdown-item" href="#">
                    <i class="fa fa-wrench"></i>{{__('Settings')}}</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{!! route('admin.logout') !!}" class="btn btn-default btn-flat"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="fa fa-lock"></i>{{__('Logout')}}
                </a>
                <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </li>
    </ul>
</header>

<div class="app-body">
    @include('admins.layouts.sidebar')
    <main class="main">
        @yield('content')
    </main>
</div>
<footer class="app-footer">
    <div>
        {{--<a href="https://infyom.com">InfyOm </a>--}}
        {{--<span>&copy; 2019 InfyOmLabs.</span>--}}
        <span>info@unionpourlarepublique.tg</span>
    </div>
    <div class="ml-auto">
        <span>Smart-Unir</span>
        {{--<span>Powered by</span>
        <a href="https://coreui.io">CoreUI</a>--}}
    </div>
</footer>
</body>
<!-- jQuery 3.1.1 -->
<script src="{{ asset('template/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('template/js/popper.min.js') }}"></script>
<script src="{{ asset('template/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('template/js/moment.min.js') }}"></script>
<script src="{{ asset('template/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('template/js/coreui.min.js') }}"></script>
@yield('scripts')

</html>
