<li class="nav-item {{ Request::is('admin/buttons*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin.buttons.index') }}">
        <i class="nav-icon fa fa-link" style="color: blue"></i>
        <span>{{__('Buttons')}}</span>
    </a>
</li>
<li class="nav-item {{ Request::is('admin/slides*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin.slides.index') }}">
        <i class="nav-icon fa fa-image" style="color: blue"></i>
        <span>Slides</span>
    </a>
</li>
<li class="nav-item {{ Request::is('admin/newsletters*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin.newsletters.index') }}">
        <i class="nav-icon fa fa-at" style="color: blue"></i>
        <span>Newsletters</span>
    </a>
</li>
<li class="nav-item {{ Request::is('admin/newsletterMessages*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin.newsletterMessages.index') }}">
        <i class="nav-icon fa fa-envelope" style="color: blue"></i>
        <span>Newsletter Messages</span>
    </a>
</li>
<li class="nav-item {{ Request::is('admin/parameters*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin.parameters.index') }}">
        <i class="nav-icon fa fa-cog" style="color: blue"></i>
        <span>Parameters</span>
    </a>
</li>
