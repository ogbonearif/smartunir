<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            @include('admins.layouts.menu')
        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
