<!-- Datatables -->
<script src="{{ asset('template/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('template/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('template/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('template/js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('template/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script>
