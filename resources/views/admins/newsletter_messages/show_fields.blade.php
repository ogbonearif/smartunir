<!-- Title Field -->
<div class="form-group">
    <h2>{!! __('Subject:') !!}</h2>
    <p>{{ $newsletterMessage->subject }}</p>
</div>

<hr>
<!-- Description Field -->
<div class="form-group">
    <h2>{!! __('Message:') !!}</h2>
    <p>{!! $newsletterMessage->message !!}</p>
    <hr>
</div>

<hr>

@if(config('app.locale') == "fr")
    <p style="color: blue">
        {{ date("d-m-Y", strtotime($newsletterMessage->created_at)) . ' à ' . date("H:i:s",strtotime($newsletterMessage->created_at))}}
    </p>
@else
    <p style="color: blue">
        {{ date("Y-m-d", strtotime($newsletterMessage->created_at)) . ' at ' . date("H:i:s",strtotime($newsletterMessage->created_at))}}
    </p>
@endif


