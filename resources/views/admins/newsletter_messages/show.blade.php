@extends('admins.layouts.app')

@section('title')
    {{__('Detail')}} message
@endsection

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('admin.newsletterMessages.index') }}">Newsletter Message</a>
            </li>
            <li class="breadcrumb-item active">{{__('Detail')}}</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>{{__('Details')}}</strong>
                                  <a href="{{ route('admin.newsletterMessages.index') }}" class="btn btn-light">{{__('Back')}}</a>
                             </div>
                             <div class="card-body">
                                 @include('admins.newsletter_messages.show_fields')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection
