<!-- Title Field -->
<div class="form-group col-sm-8">
    {!! '<b>'. Form::label('subject', __('Subject:')) . '</b>' !!}
    {!! Form::text('subject', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-8 col-lg-8">
    {!! '<b>'. Form::label('message', __('Message:')) . '</b>' !!}
    {!! Form::textarea('message', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('Send'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('admin.newsletterMessages.index') }}" class="btn btn-secondary">{{__('Cancel')}}</a>
</div>

@section('scripts')
<script src="https://colorlib.com/polygon/adminty/files/assets/pages/wysiwyg-editor/js/tinymce.min.js" type="a0a26a3bfed01469b5ea9793-text/javascript"></script>
<script src="https://colorlib.com/polygon/adminty/files/assets/pages/wysiwyg-editor/wysiwyg-editor.js" type="a0a26a3bfed01469b5ea9793-text/javascript"></script>
<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="a0a26a3bfed01469b5ea9793-|49" defer=""></script>
@endsection
