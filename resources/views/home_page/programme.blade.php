@extends('home_page.layout_campagnes')

@section('title')
    Smart-Unir | Programme
@endsection

@section('h2_title')
    Smart-Unir Programme
@endsection

@section('content')
    <section class="ls section_padding_top_100 section_padding_bottom_100 blue_background">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div id="carouselprogramme" class="carousel alignright" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="item active">
                                <img src="{{ asset("home_page/images/gallery/programme/programme1.jpg") }}" alt="">
                            </div>
                            <div class="item">
                                <img src="{{ asset("home_page/images/gallery/programme/programme2.jpg") }}" alt="">
                            </div>
                            <div class="item">
                                <img src="{{ asset("home_page/images/gallery/programme/programme3.jpg") }}" alt="">
                            </div>
                            <div class="item">
                                <img src="{{ asset("home_page/images/gallery/programme/programme4.jpg") }}" alt="">
                            </div>
                            <div class="item">
                                <img src="{{ asset("home_page/images/gallery/programme/programme5.jpg") }}" alt="">
                            </div>
                            <div class="item">
                                <img src="{{ asset("home_page/images/gallery/programme/programme6.jpg") }}" alt="">
                            </div>
                        </div>
                        <a class="left carousel-control-prev" href="#carouselprogramme"
                           data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control-next" href="#carouselprogramme"
                           data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                  {{-- <img src="{{ asset("home_page/images/gallery/programme/programme1.jpg") }}" alt=""
                                            class="alignright">--}}
                    <h2 class="section_header topmargin_0 h2_police">Smart-UNIR Programme</h2>
                    {{--                    <h4 class="entry-title small"></h4>--}}
                    <h4><b>Axe 1</b> : Croire</h4>
                    <p class="p_color">
                        D’abord la Croyance en Dieu (pour les Croyants), Lui qui permet tout et dont l’invocation
                        fera rejaillir ses grâces et la paix sur notre Nation.
                    </p>
                    <p class="p_color">
                        Ensuite la Croyance en nous-mêmes, en la force de nos efforts collectifs, en
                        l’avenir du Togo, en nos potentialités et capacités, etc.
                    </p>
                    <p class="p_color">
                        <b>CROIRE</b> est aussi une invite adressée au monde à Croire au génie transformationnel du
                        peuple togolais qui fait ses preuves dans plusieurs domaines sous le leadership de SEM
                        Faure <b>GNASSINGBÉ</b>.
                    </p>
                    <h5><em>Signature invariable de la campagne :</em></h5>
                    <h6><b>ALLONS-Y FAURE!</b></h6>
                    <p class="p_color">
                        Le peuple invite ainsi affectueusement le président Faure à poursuivre de plus bel ce qu’il
                        a commencé tout en lui faisant la promesse d’être à ses côtés pour cette nouvelle aventure.
                    </p>
                    <h4><b>Axe 2 </b>: Osons Davantage</h4>
                    <p class="p_color">
                        Nous avons fait du chemin, enregistré des grands succès, nous avons un bilan qui témoigne
                        pour nous et nous encourage à consolider l’existant et à accélérer ensemble la prospérité de
                        notre pays avec audace, courage et abnégation.
                    </p>
                    <p class="p_color">
                        La promesse de prospérité et de son partage à toutes les couches sociales du Togo est donc
                        bel et bien possible pour bâtir un Togo émergent avec Faure <b>GNASSINGBÉ</b>.
                    </p>
                    <h5><em>Signature invariable de la campagne :</em></h5>
                    <h6><b>ALLONS-Y FAURE!</b></h6>
                    <p class="p_color">
                        Le peuple invite ainsi affectueusement le président Faure à poursuivre de plus bel ce qu’il
                        a commencé tout en lui faisant la promesse d’être à ses côtés pour cette nouvelle aventure.
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection
