@extends('home_page.layout_campagnes')

@section('title')
   Smart-UNIR | Mot du président
@endsection

@section('h2_title')
    Mot du président
@endsection

@section('content')
    <section class="ls {{--section_padding_top_100--}} section_padding_bottom_100 blue_background">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">{{--<img src="{{ asset("home_page/images/gallery/slide_bureautique.jpg") }}" alt=""
                                            class="alignright">--}}
                    <h2 class="section_header topmargin_0 h2_police">Cher militant et cher concitoyen,</h2>
                    <h4 class="entry-title small">Togolaise ma sœur, Togolais mon frère,</h4>
{{--                    <p>Togolaise ma sœur, Togolais mon frère,</p>--}}
                    <p class="p_color">
                        Par cette belle journée porteuse d’espoir, j’ai l’immense plaisir de te souhaiter la bienvenue
                        chez nous, chez toi, à Smart-UNIR, une plateforme collaborative avec un portail digital qui
                        s’ouvre à nous. Puisque que tu es là, je me permets de te parler de nous et de toi de UNIR,
                        l’Union pour la République, de notre idéal commun.
                    </p>
                    <p class="p_color">
                        Au moment de notre naissance, nous nous sommes inscrits dans un tissu relationnel,
                        nous constituant à la fois comme individu singulier, mais surtout comme membre d’une communauté
                        agissante. Cette communauté se distingue à des échelles, d’où notre regroupement en famille,
                        notre appartenance à un même groupe, vivant au cœur d’un village ou d’une ville se trouvant
                        dans une région, localisée dans un pays, etc.
                    </p>
                    <p class="p_color">
                        Dans cette logique, se sont construites nos civilisations, qui se sont différenciées petit à
                        petit par leurs convictions, leurs modes de pensée et de perception de l’autre. Un élément nous
                        rassemble cependant. Sans exception, nous sommes tous héritiers d’un même patrimoine,
                        la Terre de nos aïeux : le Togo. Mieux encore, nous sommes tous préoccupés par son rayonnement.
                    </p>
                    <p class="p_color">
                        Notre culture, nos traditions, notre mode de pensée sont autant de richesses qui constituent
                        notre bagage personnel, et ce bagage ne cesse de s’enrichir à l’aune de nos rencontres. Pourtant,
                        certains le gardent encore fermé à double tour; par simple précaution parfois, et souvent par peur
                        d’être lésés.
                    </p>
                    <p class="p_color">
                        Sachons-le, le repli sur soi-même est une prison; mais l’échange est vital et le brassage est un
                        enrichissement mutuel.
                    </p>
                    <p class="p_color">
                        Ainsi, entre égoïsme et altruisme, identité et altérité, individualisme et communautarisme, 
                        UNIR s’est distingué par sa capacité à rassembler autour d’un objectif commun : développer le Togo.
                        Oscar WILD ne disait-il pas que « Aimer, c’est se surpasser » ? Aime donc le Togo et accepte de
                        le bâtir ensemble avec nous.
                    </p>
                    <p class="p_color">
                        Si tu es déjà membre de la famille, impliques-toi davantage; si tu ne l’es pas encore,
                        rejoins-nous assez vite. Car, chaque jour compte dans la construction du pays dont nous rêvons
                        et ta participation accélèrera le chantier. Afin d’éviter les critiques d’après Aristote,
                        il faut « ne rien dire, ne rien faire et n’être rien ». Or ici, nous cultivons la tolérance,
                        l’égalité, le respect mutuel, et la solidarité agissante, nous faisons le développement avec
                        la collaboration de tous, et nous sommes engagés pour un Togo prospère.
                    </p>
                    <p class="p_color">
                        En toute confiance, apprenons à nous en sortir grâce à Smart-UNIR, apprenons de nos expériences
                        mutuelles, saisissons-y les opportunités. Le Togo peut s’enrichir de nos différences.
                    </p>
                    <p class="p_color">
                        Soyons juste unis et créons ensemble le Togo de nos rêves.
                    </p>
                    <p class="p_color">Si tu y crois aussi, Allons-y !</p>
                    <p class="p_color">Avec Smart-UNIR tu peux t’en sortir.</p>
                    <h3 class="entry-title small">Faure Essozimna Gnassingbé</h3>
                </div>
            </div>
        </div>
    </section>
@endsection
