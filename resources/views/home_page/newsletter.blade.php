<div class="col-xs-12">
    <div id="subscribe" class="widget widget_mailchimp with_padding cs main_color3 parallax overlay_color topmargin_0">
        <h3 class="widget-title">Notre Newsletter</h3>
        <form method="get" action="{{ route('store.newsletter') }}" >
            <div class="form-group-wrap">
                <div class="form-group">
                    <label for="email" class="sr-only">Entrer votre email</label>
                    <input name="Email" type="email" id="email"
                           class="mailchimp_email form-control text-center news-mail {{$errors->has('Email')?'is-invalid':'' }}" placeholder="Entrer votre email">
                    @if ($errors->has('Email'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('Email') }}</strong>
                        </span>
                    @endif
                </div>
                <button type="submit" class="theme_button color4">Souscrire maintenant</button>
            </div>
            <div class="response"></div>
        </form>
    </div>
</div>
