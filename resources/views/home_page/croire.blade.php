@extends('home_page.layout_campagnes')

@section('title')
    Smart-Unir | Croire En Notre Togo
@endsection

@section('h2_title')
    Croire En Notre Togo
@endsection

@section('content')
    <section class="ls section_padding_top_100 section_padding_bottom_100 blue_background">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div id="carouselprogramme" class="carousel alignright" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="item active">
                                <img src="{{ asset("home_page/images/gallery/croire/croire1.jpeg") }}" alt="">
                            </div>
                            <div class="item">
                                <img src="{{ asset("home_page/images/gallery/croire/croire2.jpg") }}" alt="">
                            </div>
                            <div class="item">
                                <img src="{{ asset("home_page/images/gallery/croire/croire3.jpg") }}" alt="">
                            </div>
                            <div class="item">
                                <img src="{{ asset("home_page/images/gallery/croire/croire4.jpg") }}" alt="">
                            </div>
                            <div class="item">
                                <img src="{{ asset("home_page/images/gallery/croire/croire5.jpg") }}" alt="">
                            </div>
                        </div>
                        <a class="left carousel-control-prev" href="#carouselprogramme"
                           data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control-next" href="#carouselprogramme"
                           data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                    <h2 class="section_header topmargin_0 h2_police">Croire En Notre Togo</h2>
                    {{--                    <h4 class="entry-title small"></h4>--}}
                    <p class="p_color">
                        D’abord la Croyance en Dieu (pour les Croyants), Lui qui permet tout et dont l’invocation
                        fera rejaillir ses grâces et la paix sur notre Nation.
                    </p>
                    <p class="p_color">
                        Ensuite la Croyance en nous-mêmes, en la force de nos efforts collectifs, en
                        l’avenir du Togo, en nos potentialités et capacités, etc.
                    </p>
                    <p class="p_color">
                        <b>CROIRE</b> est aussi une invite adressée au monde à Croire au génie transformationnel du
                        peuple togolais qui fait ses preuves dans plusieurs domaines sous le leadership de SEM
                        Faure <b>GNASSINGBÉ</b>.
                    </p>
                    <h5><em>Signature invariable de la campagne :</em></h5>
                    <h6><b>ALLONS-Y FAURE!</b></h6>
                    <p class="p_color">
                        Le peuple invite ainsi affectueusement le président Faure à poursuivre de plus bel ce qu’il
                        a commencé tout en lui faisant la promesse d’être à ses côtés pour cette nouvelle aventure.
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection
