@extends('home_page.layout_campagnes')

@section('title')
    Smart-Unir | Challenge
@endsection

@section('h2_title')
    Smart-Unir Challenge
@endsection

@section('content')
    <section class="ls {{--section_padding_top_100--}} section_padding_bottom_100 blue_background">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">{{--<img src="{{ asset("home_page/images/gallery/slide_bureautique.jpg") }}" alt=""
                                            class="alignright">--}}
                    <h2 class="section_header topmargin_0 h2_police">Smart-UNIR CHALLENGE</h2>
{{--                    <h4 class="entry-title small"></h4>--}}
                    <p class="p_color">
                        Smart-UNIR CHALLENGE est ouvert à tous les visiteurs, sympathisants et les partisans du grand parti UNIR,
                        non seulement désireux de s’informer de tous les programmes de campagnes au niveau de chaque préfecture
                        mais aussi de participer numériquement à la campagne présidentielle du candidat FAURE.
                    </p>
                    <p class="p_color">
                        Le challenge se déroulera du 06 au 22 Février 2020, en deux catégories :
                    </p>
                   <h5><b>Catégorie A</b> : Meilleur Animateur numérique</h5>
                    <p class="p_color">
                        Elle consiste à rester informé, Liker, Commenter et Partager au maximum les articles reçus sur
                        son application Smart-UNIR, cumulant ainsi des points selon le barème suivant :
                    </p>
                    <ul class="p_color">
                        <li>Liker (Point = 1)</li>
                        <li>Commenter (Point = 1)</li>
                        <li>Partager (Point = 3)</li>
                    </ul>
                    <h5><b>Catégorie B </b> : Meilleur Reporteur Vidéo, Image et Son</h5>
                    <p class="p_color">Elle consiste à enregistrer et sélectionner chaque jour la meilleure vidéo,
                        images ou son dans les différentes manifestations de campagnes du grand parti UNIR puis les
                        envoyer sur Smart-UNIR à travers le menu Media.
                    </p>
                    <h3>RECOMPENSES ATTENDUES :</h3>
                    <p class="p_color">Les meilleurs de chaque catégorie recevront :</p>
                    <ul class="p_color">
                        <li> <b>Quotidiennement</b> : un forfait data à la fin de la journée pour les meilleurs candidats
                            qui se serait démarqués au courant de la journée.
                        </li>
                        <li> <b>A la fin du Challenge </b> :
                            <ul class="p_color">
                                <li>une distinction honorifique </li>
                                <li>un certificat de gratitude signé des mains du Président FAURE </li>
                                <li>une invitation à la cérémonie d’investiture du président FAURE pour son
                                    mandat prochain</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
@endsection
