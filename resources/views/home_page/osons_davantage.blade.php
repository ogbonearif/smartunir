@extends('home_page.layout_campagnes')

@section('title')
    Smart-Unir | Osons Davantage
@endsection

@section('h2_title')
    Osons Davantage
@endsection

@section('content')
    <section class="ls section_padding_top_100 section_padding_bottom_100 blue_background">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                   {{-- <img src="{{ asset("home_page/images/gallery/osons/osons1.jpeg") }}" alt=""
                         class="alignright">--}}
                    <div id="carouselprogramme" class="carousel alignright" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="item active">
                                <img src="{{ asset("home_page/images/gallery/osons/osons1.jpeg") }}" alt="">
                            </div>
                            <div class="item">
                                <img src="{{ asset("home_page/images/gallery/osons/osons2.jpg") }}" alt="">
                            </div>
                            <div class="item">
                                <img src="{{ asset("home_page/images/gallery/osons/osons3.jpg") }}" alt="">
                            </div>
                        </div>
                        <a class="left carousel-control-prev" href="#carouselprogramme"
                           data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control-next" href="#carouselprogramme"
                           data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>

                    <h2 class="section_header topmargin_0 h2_police">Osons Davantage</h2>
                    {{--                    <h4 class="entry-title small"></h4>--}}
                    <p class="p_color">
                        Nous avons fait du chemin, enregistré des grands succès, nous avons un bilan qui témoigne
                        pour nous et nous encourage à consolider l’existant et à accélérer ensemble la prospérité de
                        notre pays avec audace, courage et abnégation.
                    </p>
                    <p class="p_color">
                        La promesse de prospérité et de son partage à toutes les couches sociales du Togo est donc
                        bel et bien possible pour bâtir un Togo émergent avec Faure <b>GNASSINGBÉ</b>.
                    </p>
                    <h5><em>Signature invariable de la campagne :</em></h5>
                    <h6><b>ALLONS-Y FAURE!</b></h6>
                    <p class="p_color">
                        Le peuple invite ainsi affectueusement le président Faure à poursuivre de plus bel ce qu’il
                        a commencé tout en lui faisant la promesse d’être à ses côtés pour cette nouvelle aventure.
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection
