<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<!-- Mirrored from webdesign-finder.com/html/diversify/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 01 Dec 2019 21:11:12 GMT -->
<head>
    <title>Smart-UNIR</title>
    <link rel="icon" href="{{ asset("home_page/images/logo_unir.jpg")}}">

    <meta charset="utf-8">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="stylesheet" href="{{ asset("home_page/css/bootstrap.min.css") }}">
    <link rel="stylesheet" href="{{ asset("home_page/css/animations.css") }}">
    <link rel="stylesheet" href="{{ asset("home_page/css/fonts.css") }}">
    <link rel="stylesheet" href="{{ asset("home_page/css/main.css") }}" class="color-switcher-link">
    <link rel="stylesheet" href="{{ asset("home_page/css/shop.css") }}" class="color-switcher-link">
    <link rel="stylesheet" href="{{ asset("template/css/icon_flashy.css") }}">

    <script src="{{ asset("home_page/js/vendor/modernizr-2.6.2.min.js") }}"></script>
<!--[if lt IE 9]>
    <script src="{{ asset('home_page/js/vendor/html5shiv.min.js') }}"></script>
    <script src="{{ asset('home_page/js/vendor/respond.min.js') }}"></script>
    <script src="{{ asset('home_page/js/vendor/jquery-1.12.4.min.js') }}"></script>
    <![endif]-->
    <link href="{{ asset('template/css/icon_flashy.css') }}" rel="stylesheet">
    <style>
        /* Make the image fully responsive */
        .carousel-inner img {
            width: 100%;
            height: 100%;
        }
    </style>
</head>

<body>
{{--<!--[if lt IE 9]>
<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" class="highlight">upgrade your browser</a> to improve your experience.</div>
<![endif]-->--}}

<div class="preloader">
    <div class="preloader_image"></div>
</div>
<!-- search modal -->
<div class="modal" tabindex="-1" role="dialog" aria-labelledby="search_modal" id="search_modal">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">
			<i class="rt-icon2-cross2"></i>
		</span>
    </button>
    <div class="widget widget_search">
        <form method="get" class="searchform search-form form-inline"
              action="http://webdesign-finder.com/html/diversify/">
            <div class="form-group bottommargin_0">
                <input type="text" value="" name="search" class="form-control" placeholder="Search keyword"
                       id="modal-search-input">
            </div>
            <button type="submit" class="theme_button no_bg_button">Search</button>
        </form>
    </div>
</div>
<!-- Unyson messages modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="messages_modal">
    <div class="fw-messages-wrap ls with_padding">
        <!-- Uncomment this UL with LI to show messages in modal popup to your user: -->
        <!--
    <ul class="list-unstyled">
        <li>Message To User</li>
    </ul>
    -->
    </div>
</div>
<!-- eof .modal -->
<!-- wrappers for visual page editor and boxed version of template -->
<div id="canvas">
    <div id="box_wrapper">
        <!-- template sections -->
        <header class="page_header header_white toggler_xs_right columns_margin_0">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12 display_table">
                        <div class="header_left_logo display_table_cell">
                            <a href="{{ route('home.page') }}" class="logo logo_with_text">
                                <img src="{{ asset("home_page/images/logo_unir.jpg")}}" alt="">
                                <span class="logo_text">
                                    Smart-Unir
                                    {{--<small class="highlight4">Suivi Scolaire</small>--}}
                                </span>
                            </a>
                        </div>
                        <div class="header_mainmenu display_table_cell text-center">
                            <!-- main nav start -->
                            <nav class="mainmenu_wrapper">
                                <ul class="mainmenu nav sf-menu">
                                    {{--<li class="active"><a href="">Accueil</a></li>--}}
                                    <li><a href="#about">À propos</a></li>
                                    <li><a href="#plateforme">Plateformes</a></li>
                                    <li><a href="#campagne">Campagne 2020</a></li>
                                    <li><a href="#unir-mouvements">UNIR & Mouvements</a></li>
                                    <li><a href="#gallery">Galerie</a></li>
                                    <li><a href="#subscribe">Newsletter</a></li>
                                </ul>
                            </nav>
                            <!-- eof main nav -->
                            <!-- header toggler --><span class="toggle_menu"><span></span></span>
                        </div>
                        @if($visible_btn)
                            <div class="header_right_buttons display_table_cell text-right hidden-xs">
                                <a href="{{ $visible_btn->link }}" target="_blank"
                                   class="{{'theme_button margin_0 ' . $visible_btn->color}}">{{ $visible_btn->label }}</a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </header>

        <section class="intro_section page_mainslider ds all-scr-cover bottom-overlap-teasers">
            <div class="flexslider" data-dots="true" data-nav="true">
                <ul class="slides">
                    @if(empty($slides[0]))
                        <li>
                            <div class="slide-image-wrap">
                                <img src="{{ asset('home_page/images/default_slide.jpg') }}" alt=""/>
                            </div>
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-12 text-center">
                                        <div class="slide_description_wrapper">
                                            <div class="slide_description">
                                                <div class="intro-layer" data-animation="fadeInRight">

                                                </div>
                                                <div class="intro-layer" data-animation="fadeInLeft">
                                                    <h2><span
                                                            class="highlight3">Titre 1 par défaut</span><br> Titre 2 par
                                                        défaut
                                                    </h2>
                                                </div>
                                                <div class="intro-layer" data-animation="fadeInUp">
                                                    <div class="slide_buttons">
                                                        <a href="#" class="theme_button color1 min_width_button">Be
                                                            Proud!</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- eof .slide_description -->
                                        </div>
                                        <!-- eof .slide_description_wrapper -->
                                    </div>
                                    <!-- eof .col-* -->
                                </div>
                                <!-- eof .row -->
                            </div>
                            <!-- eof .container -->
                        </li>
                    @else
                        @foreach($slides as $slide)
                            <li>
                                <div class="slide-image-wrap">
                                    <img src="{{ $slide->image_url }}" alt=""/>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12 text-center">
                                            <div class="slide_description_wrapper">
                                                <div class="slide_description">
                                                    <div class="intro-layer" data-animation="fadeInRight">

                                                    </div>
                                                    <div class="intro-layer" data-animation="fadeInLeft">
                                                        <h2><span
                                                                class="highlight3">{{ $slide->title1 }}</span><br> {{ $slide->title2 }}
                                                        </h2>
                                                    </div>
                                                    @if($slide->button_label && $slide->button_link)
                                                        <div class="intro-layer" data-animation="fadeInUp">
                                                            <div class="slide_buttons">
                                                                <a href="{{ $slide->button_link }}"
                                                                   class="{{"theme_button ". $slide->button_color ." min_width_button"}}">{{ $slide->button_label }}</a>
                                                            </div>
                                                        </div>
                                                    @endif

                                                </div>
                                                <!-- eof .slide_description -->
                                            </div>
                                            <!-- eof .slide_description_wrapper -->
                                        </div>
                                        <!-- eof .col-* -->
                                    </div>
                                    <!-- eof .row -->
                                </div>
                                <!-- eof .container -->
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>
            <!-- eof flexslider -->
        </section>
        <section id="services" class="ls section_intro_overlap columns_padding_0 columns_margin_0 container_padding_0">
            <div class="container-fluid">
                <div class="row flex-wrap v-center-content">
                    <div class="col-lg-3 col-sm-6 col-xs-12 to_animate" data-animation="fadeInUp">
                        <div class="teaser main_bg_color transp with_padding big-padding margin_0">
                            <div class="media xxs-media-left">
                                <div class="media-left media-middle">
                                    <div
                                        class="teaser_icon size_small round light_bg_color thick_border_icon big_wrapper">
                                        <i class="fa fa-download highlight4" aria-hidden="true"></i></div>
                                </div>
                                <div class="media-body media-middle">
                                    <h4><a href="http://unionpourlarepublique.tg/app">Téléchager Smart-UNIR</a></h4>
                                    <p>Application disponible pour ANDROID.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-xs-12 to_animate" data-animation="fadeInUp">
                        <div class="teaser main_bg_color2 transp with_padding big-padding margin_0">
                            <div class="media xxs-media-left">
                                <div class="media-left media-middle">
                                    <div
                                        class="teaser_icon size_small round light_bg_color2 thick_border_icon big_wrapper">
                                        <i class="fa fa-eye highlight5" aria-hidden="true"></i></div>
                                </div>
                                <div class="media-body media-middle cadrant_2_color">
                                    <h4 class="cadrant_2_color"><a href="" style="color:#009ee2;">Signature de Campagne</a></h4>
                                    <p>Allons-Y FAURE.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-xs-12 to_animate" data-animation="fadeInUp">
                        <div class="teaser main_bg_color transp with_padding big-padding margin_0">
                            <div class="media xxs-media-left">
                                <div class="media-left media-middle">
                                    <div
                                        class="teaser_icon size_small round light_bg_color thick_border_icon big_wrapper">
                                        {{--                                        <i class="fa fa-users highlight3" aria-hidden="true"></i></div>--}}
                                        <i class="fa fa-street-view highlight4" aria-hidden="true"></i></div>
                                </div>
                                <div class="media-body media-middle">
                                    <h4><a href="{{ route('croire') }}">Croire</a></h4>
                                    <p>les multiples raisons du choix de FAURE pour le mandat prochain.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-xs-12 to_animate" data-animation="fadeInUp">
                        <div class="teaser main_bg_color2 transp with_padding big-padding margin_0">
                            <div class="media xxs-media-left">
                                <div class="media-left media-middle">
                                    <div
                                        class="teaser_icon size_small round light_bg_color2 thick_border_icon big_wrapper">
                                        {{--                                   <i class="fa fa-briefcase highlight4" aria-hidden="true"></i></div>--}}
                                        <i class="fa fa-eye highlight5" aria-hidden="true"></i></div>
                                </div>
                                <div class="media-body media-middle cadrant_2_color">
                                    <h4><a href="{{ route('osons') }}" style="color:#009ee2;">Osons Davantage</a></h4>
                                    <p>Toute la campagne en images.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="about" class="ls section_padding_top_110 columns_padding_30 blue_background">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-push-6 to_animate" data-animation="fadeInUp" data-delay="600">
                        <div class="embed-responsive embed-responsive-3by2">
                            <a href="https://www.youtube.com/embed/IJF5KW-GV2s" class="embed-placeholder">
                                <img src="{{ asset("home_page/images/gallery/F02.jpg")}}" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6 col-md-pull-6 to_animate" data-animation="fadeInRight" data-delay="300">
                        <h2 class="section_header color4"> À propos </h2>
                        <p class="section-excerpt grey">Smart-UNIR est un outil collaboratif dédié aux visiteurs,
                            sympathisants et partisans du plus grand parti politique togolais : UNIR</p>
                        <p>C’est une plateforme web et mobile qui offre différents niveau de service dont le principal
                            est de tenir tous les camarades au même niveau d’information. Elle donne la possibilité
                            d’échanges entre les différents dirigeants du parti et les camarades membres. Elle ouvre
                            également des liens vers les différentes plateformes.
                        </p>
                        <p>
                            Smart-UNIR offre également une bibliothèque virtuelle sur les documents d’archives
                            politiques
                            du TOGO, et des formations politiques.
                        </p>
                        <p>
                            Pour cette période de campagne présidentielle 2020, Smart-UNIR est dédiée uniquement qu’aux
                            différentes
                            manifestations de campagnes et CHALLENGES :
                        </p>
                        <ul>
                            <li>Meilleur animateur numérique</li>
                            <li>Meilleur reporteur amateur</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section id="plateforme"
                 class="ls section_padding_top_100 section_padding_bottom_100 columns_margin_bottom_20 blue_background">
            <div class="container" style="padding-bottom: 0">
                <div class="row flex-wrap">
                    <div class="col-sm-12 to_animate" data-animation="fadeInLeft">
                        <h2 class="section_header">Plateformes</h2>
                    </div>
                    <div class="col-md-4 col-sm-6 to_animate" data-animation="fadeInDown">
                        <article
                            class="vertical-item content-padding big-padding with_border bottom_color_border {{--loop-color--}} text-center">
                            <div class="item-media">
                                <img src="{{ asset("home_page/images/www_unir_tg.jpeg")}}" alt="">
                            </div>
                            <div class="item-content">
                                <header class="entry-header">
                                    <h3 class="entry-title small">
                                        <a href="http://unir.tg" target="_blank">www.unir.tg</a>
                                    </h3>
                                    <span class="small-text highlight">{{--leader--}}</span>
                                </header>
                                <p class="member-social greylinks">
                                    <a class="social-icon socicon-facebook" target="_blank"
                                       href="https://www.facebook.com/UNIRvote/" title="Facebook"></a>

                                    <a class="social-icon socicon-twitter" target="_blank"
                                       href="https://twitter.com/UnirVote?s=08" title="Twitter"></a>

                                    <a class="social-icon socicon-youtube" target="_blank"
                                       href="https://www.youtube.com/channel/UC4xZ77411ZdUl_TY_gWXd5g" title="Youtube"></a>
                                </p>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4 col-sm-6 to_animate" data-animation="fadeInDown">
                        <article
                            class="vertical-item content-padding big-padding with_border bottom_color_border {{--loop-color--}} text-center">
                            <div class="item-media">
                                <img src="{{ asset("home_page/images/faure_president_com.jpeg")}}" alt="">
                            </div>
                            <div class="item-content">
                                <header class="entry-header">
                                    <h3 class="entry-title small" style="font-size: 22.3px">
                                        <a href="http://faurepresident.com" target="_blank">www.faurepresident.com</a>
                                    </h3>
                                    <span class="small-text highlight">{{--assistant--}}</span>
                                </header>
                                <p class="member-social greylinks">
                                    <a class="social-icon socicon-facebook" target="_blank"
                                       href="https://www.facebook.com/UNIRvote/" title="Facebook"></a>

                                    <a class="social-icon socicon-twitter" target="_blank"
                                       href="https://twitter.com/UnirVote?s=08" title="Twitter"></a>

                                    <a class="social-icon socicon-youtube" target="_blank"
                                       href="https://www.youtube.com/channel/UC4xZ77411ZdUl_TY_gWXd5g" title="Youtube"></a>
                                </p>
                            </div>
                        </article>
                    </div>

                    <div class="col-md-4 col-sm-6 to_animate" data-animation="fadeInDown">
                        <article
                            class="vertical-item content-padding big-padding with_border bottom_color_border {{--loop-color--}} text-center">
                            <div class="item-media">
                                <img src="{{ asset("home_page/images/reseaux_sociaux.jpg")}}" alt="">
                            </div>
                            <div class="item-content">
                                <header class="entry-header">
                                    <h3 class="entry-title small ">
                                        <a href="#" target="_blank">Réseaux Sociaux</a>
                                    </h3>
                                    <span class="small-text highlight">{{--assistant--}}</span>
                                </header>
                                <p class="member-social greylinks">
                                    <a class="social-icon socicon-facebook" target="_blank"
                                       href="https://www.facebook.com/UNIRvote/" title="Facebook"></a>

                                    <a class="social-icon socicon-twitter" target="_blank"
                                       href="https://twitter.com/UnirVote?s=08" title="Twitter"></a>

                                    <a class="social-icon socicon-youtube" target="_blank"
                                       href="https://www.youtube.com/channel/UC4xZ77411ZdUl_TY_gWXd5g" title="Youtube"></a>
                                </p>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>

        <section id="campagne"
                 class="ls section_padding_top_100 section_padding_bottom_100 columns_margin_bottom_20 blue_background">
            <div class="container" style="padding-bottom: 0">
                <div class="row flex-wrap">
                    <div class="col-sm-12 to_animate" data-animation="fadeInLeft">
                        <h2 class="section_header">Campagne 2020</h2>
                    </div>
                    <div class="col-md-4 col-sm-6 to_animate" data-animation="fadeInDown">
                        <article
                            class="vertical-item content-padding big-padding with_border bottom_color_border text-center">
                            <div class="item-media">
                                <img src="{{ asset("home_page/images/mot_PR.jpg")}}" alt="">
                            </div>
                            <div class="item-content">
                                <header class="entry-header">
                                    <h3 class="entry-title small">
                                        <a href="{{ route('mot.president')}}">Mot du président</a>
                                    </h3>
                                    <span class="small-text highlight">{{--leader--}}</span>
                                </header>
                                <p class="member-social greylinks">
                                    <a class="social-icon socicon-facebook" target="_blank"
                                       href="https://www.facebook.com/UNIRvote/" title="Facebook"></a>

                                    <a class="social-icon socicon-twitter" target="_blank"
                                       href="https://twitter.com/UnirVote?s=08" title="Twitter"></a>

                                    <a class="social-icon socicon-youtube" target="_blank"
                                       href="https://www.youtube.com/channel/UC4xZ77411ZdUl_TY_gWXd5g" title="Youtube"></a>
                                </p>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4 col-sm-6 to_animate" data-animation="fadeInDown">
                        <article
                            class="vertical-item content-padding big-padding with_border bottom_color_border text-center">
                            <div class="item-media">
                                <img src="{{ asset("home_page/images/programme.jpg") }}" alt="">
                            </div>
                            <div class="item-content">
                                <header class="entry-header">
                                    <h3 class="entry-title small">
                                        <a href="{{ route('programme.campagne') }}">Programmes</a>
                                    </h3> <span class="small-text highlight">{{--assistant--}}</span></header>
                                <p class="member-social greylinks">
                                    <a class="social-icon socicon-facebook" target="_blank"
                                       href="https://www.facebook.com/UNIRvote/" title="Facebook"></a>

                                    <a class="social-icon socicon-twitter" target="_blank"
                                       href="https://twitter.com/UnirVote?s=08" title="Twitter"></a>

                                    <a class="social-icon socicon-youtube" target="_blank"
                                       href="https://www.youtube.com/channel/UC4xZ77411ZdUl_TY_gWXd5g" title="Youtube"></a>
                                </p>
                            </div>
                        </article>
                    </div>

                    <div class="col-md-4 col-sm-6 to_animate" data-animation="fadeInDown">
                        <article
                            class="vertical-item content-padding big-padding with_border bottom_color_border text-center">
                            <div class="item-media">
                                <img src="{{ asset("home_page/images/partage1B.jpg") }}" alt="">
                            </div>
                            <div class="item-content">
                                <header class="entry-header">
                                    <h3 class="entry-title small ">
                                        <a href="{{ route('challenge.smart.unir') }}">Challenge Smart-UNIR</a>
                                    </h3>
                                    <span class="small-text highlight">{{--assistant--}}</span>
                                </header>
                                <p class="member-social greylinks">
                                    <a class="social-icon socicon-facebook" target="_blank"
                                       href="https://www.facebook.com/UNIRvote/" title="Facebook"></a>

                                    <a class="social-icon socicon-twitter" target="_blank"
                                       href="https://twitter.com/UnirVote?s=08" title="Twitter"></a>

                                    <a class="social-icon socicon-youtube" target="_blank"
                                       href="https://www.youtube.com/channel/UC4xZ77411ZdUl_TY_gWXd5g" title="Youtube"></a>
                                </p>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>

        <section id="unir-mouvements" class="ls section_padding_top_90 section_padding_bottom_110 blue_background">
            <div class="container">
                <div class="row flex-wrap v-center">
                    <div class="col-sm-8 to_animate" data-animation="fadeInRight">
                        <h2 class="section_header">UNIR & Mouvements</h2>
                    </div>
                    <div class="col-sm-12 topmargin_30">
                        <div{{-- class="loop-colors"--}}>
                            <article class="post format-small-image to_animate" data-animation="fadeInRight">
                                <div
                                    class="side-item side-md content-padding big-padding with_border bottom_color_border left">
                                    <div class="row">
                                        <div class="col-md-5 col-lg-4">
                                            <div class="item-media entry-thumbnail">
                                                <img src="{{ asset("home_page/images/LOGO_UNIR.jpg")}}" alt=""></div>
                                        </div>
                                        <div class="col-md-7 col-lg-8">
                                            <div class="item-content">
                                                <header class="entry-header">
                                                    <h3 class="entry-title small">
                                                        <a href="#" rel="bookmark">UNIR</a>
                                                    </h3>
                                                </header>
                                                <div class="entry-content md-content-3lines-ellipsis">
                                                    <p>UNIR est un Parti politique porté sur les fonts baptismaux
                                                        lors de son Assemblée générale constitutive du 14 avril 2012 à
                                                        Atakpamé et signifie « Union pour la République ».</p>
                                                </div>
                                                <p class="member-social greylinks" align="center">
                                                    <a class="social-icon socicon-facebook"
                                                       href=" https://www.facebook.com/UNIRvote/"
                                                       title="Facebook" target="_blank"></a>
                                                    <a class="social-icon socicon-twitter"
                                                       href="https://twitter.com/UnirVote?s=08"
                                                       title="Twitter" target="_blank"></a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </article>
                            <article class="post format-small-image topmargin_30 to_animate"
                                     data-animation="fadeInLeft">
                                <div
                                    class="side-item side-md content-padding big-padding with_border bottom_color_border right">
                                    <div class="row">
                                        <div class="col-md-5 col-lg-4">
                                            <div class="item-media entry-thumbnail"><img
                                                    src="{{ asset("home_page/images/MJU.png")}}" alt=""></div>
                                        </div>
                                        <div class="col-md-7 col-lg-8">
                                            <div class="item-content">
                                                <header class="entry-header">
                                                    <h3 class="entry-title small">
                                                        <a href="#" rel="bookmark">MJU</a>
                                                    </h3>
                                                </header>
                                                <div class="entry-content md-content-3lines-ellipsis">
                                                    <p>Mouvement des jeunes UNIR</p>
                                                </div>
                                                <p class="member-social greylinks" align="center">
                                                    <a class="social-icon socicon-facebook"
                                                       href="https://www.facebook.com/MJUnirTogo/"
                                                       title="Facebook" target="_blank"></a>
                                                    <a class="social-icon socicon-twitter"
                                                       href="https://twitter.com/Mju_Unir?s=08"
                                                       title="Twitter" target="_blank"></a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </article>
                            <article class="post format-small-image topmargin_30 to_animate"
                                     data-animation="fadeInRight">
                                <div
                                    class="side-item side-md content-padding big-padding with_border bottom_color_border left">
                                    <div class="row">
                                        <div class="col-md-5 col-lg-4">
                                            <div class="item-media entry-thumbnail"><img
                                                    src="{{ asset("home_page/images/MFU.png")}}" alt=""></div>
                                        </div>
                                        <div class="col-md-7 col-lg-8">
                                            <div class="item-content">
                                                <header class="entry-header">
                                                    <h3 class="entry-title small">
                                                        <a href="#" rel="bookmark">MFU</a>
                                                    </h3>
                                                </header>
                                                <div class="entry-content md-content-3lines-ellipsis">
                                                    <p>Mouvement des femmes UNIR</p>
                                                </div>
                                                <p class="member-social greylinks" align="center">
                                                    <a class="social-icon socicon-facebook"
                                                       href=" https://www.facebook.com/MFUtogo/"
                                                       title="Facebook" target="_blank"></a>
                                                    <a class="social-icon socicon-twitter" href="#"
                                                       title="Twitter" target="_blank"></a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <article class="post format-small-image topmargin_30 to_animate"
                                     data-animation="fadeInLeft">
                                <div
                                    class="side-item side-md content-padding big-padding with_border bottom_color_border right">
                                    <div class="row">
                                        <div class="col-md-5 col-lg-4">
                                            <div class="item-media entry-thumbnail"><img
                                                    src="{{ asset("home_page/images/MSU.jpg")}}" alt=""></div>
                                        </div>
                                        <div class="col-md-7 col-lg-8">
                                            <div class="item-content">
                                                <header class="entry-header">
                                                    <h3 class="entry-title small">
                                                        <a href="#" rel="bookmark">MSU</a>
                                                    </h3>
                                                </header>
                                                <div class="entry-content md-content-3lines-ellipsis">
                                                    <p>Mouvement des sages UNIR</p>
                                                </div>
                                                <p class="member-social greylinks" align="center">
                                                    <a class="social-icon socicon-facebook"
                                                       href=" https://www.facebook.com/MFUtogo/"
                                                       title="Facebook" target="_blank"></a>
                                                    <a class="social-icon socicon-twitter" href="#"
                                                       title="Twitter" target="_blank"></a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </article>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="gallery" class="ls section_padding_top_10 section_padding_bottom_110 blue_background">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 to_animate" data-animation="fadeInLeft">
                        <h2 class="section_header">Galerie</h2>
                    </div>
                    <div class="col-xs-12 bottommargin_0 to_animate" data-animation="fadeInUp">
                        <div class="row masonry-feed columns_padding_1">
                            <div class="col-xs-12 col-md-3">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-12">
                                        <div class="vertical-item content-absolute text-center ds">
                                            <div class="item-media">
                                                <div id="carousel1" class="carousel" data-ride="carousel">
                                                    <div class="carousel-inner">
                                                        <div class="item active">
                                                            <a href="https://www.flickr.com/photos/smart-unir/49480926417/in/album-72157712990394563/"
                                                               target="_blank">
                                                                <img src="{{ asset('home_page/images/gallery/F01.jpg') }}" alt="">
                                                            </a>
                                                            <div class="carousel-caption">
                                                                <h3>Pour MJU</h3>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <a href="https://www.flickr.com/photos/smart-unir/49480926417/in/album-72157712990394563/"
                                                               target="_blank">
                                                                <img src="{{ asset('home_page/images/MJU.png') }}" alt="">
                                                            </a>
                                                            <div class="carousel-caption">
                                                                <h3>Pour MJU</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-12">
                                        <div class="vertical-item content-absolute text-center ds">
                                            <div class="item-media">
                                                <div id="carousel2" class="carousel" data-ride="carousel">
                                                    <div class="carousel-inner">
                                                        <div class="item active">
                                                            <a href="https://www.flickr.com/photos/smart-unir/49480229373/in/album-72157712989001132/"
                                                               target="_blank">
                                                                <img src="{{ asset('home_page/images/MFU.png') }}" alt="">
                                                            </a>
                                                            <div class="carousel-caption">
                                                                <h3>Pour MFU</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="vertical-item closest-event content-absolute text-center ds">
                                    <div class="item-media">
                                        <div id="galleryCarousel" class="carousel" data-ride="carousel">
                                            <div class="carousel-inner">
                                                <div class="item active">
                                                    <a href="https://www.flickr.com/photos/186833236@N08/49480230203/in/album-72157712950785583/"
                                                       target="_blank">
                                                        <img src="{{ asset('home_page/images/gallery/All_gallery/F03.jpg') }}" alt="">
                                                    </a>
                                                </div>
                                                @foreach($all_gallery_images as $all_gallery_image)
                                                    <div class="item">
                                                        <a href="https://www.flickr.com/photos/186833236@N08/49480230203/in/album-72157712950785583/"
                                                           target="_blank">
                                                            <img src="{{ asset('home_page/images/gallery/All_gallery/'.$all_gallery_image) }}" alt="">
                                                        </a>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <a class="left carousel-control-prev" href="#galleryCarousel"
                                               data-slide="prev">
                                                <span class="glyphicon glyphicon-chevron-left"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control-next" href="#galleryCarousel"
                                               data-slide="next">
                                                <span class="glyphicon glyphicon-chevron-right"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-12">
                                        <div class="vertical-item content-absolute text-center ds">
                                            <div class="item-media">
                                                <div id="carousel3" class="carousel" data-ride="carousel">
                                                    <div class="carousel-inner">
                                                        <div class="item active">
                                                            <a href="https://www.flickr.com/photos/smart-unir/49480926967/in/album-72157712990499573/"
                                                               target="_blank">
                                                                <img src="{{ asset('home_page/images/gallery/F03.jpg') }}" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="item">
                                                            <a href="https://www.flickr.com/photos/smart-unir/49480926967/in/album-72157712990499573/"
                                                               target="_blank">
                                                                <img src="{{ asset('home_page/images/gallery/F04.jpg') }}" alt="">
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item-content darken_gradient">
                                                <h3 class="entry-title"><a href="#">Campagne 2020</a></h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-12">
                                    <div class="vertical-item content-absolute text-center ds">
                                        <div class="item-media"><img
                                                src="{{ asset('home_page/images/gallery/F01.jpg') }}" alt=""></div>
                                        <div class="item-content darken_gradient">
                                            <h3 class="entry-title"><a
                                                    href="#">{{--Turnback Tuesdays at Lips--}}</a></h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div>
    </div>
    </section>

    <footer
        class="page_footer template_footer ds ms parallax overlay_color section_padding_top_110 section_padding_bottom_100 columns_padding_25">
        <div class="container">
            <div class="row">
                @include('home_page.newsletter')
                <div class="col-md-4 col-sm-6 col-xs-12 col-md-push-4 col-md-offset-2 text-center">
                    <div class="widget widget_text widget_about">
                        <div class="logo logo_with_text bottommargin_10"><img
                                src="{{ asset("home_page/images/logo_unir.jpg")}}" alt="">
                        </div>
                        <p>UNIR est un Parti politique porté sur les fonts baptismaux
                            lors de son Assemblée générale constitutive du 14 avril 2012 à Atakpamé
                            et signifie « Union pour la République ».</p>
                        <p class="topmargin_25">
                            <a class="social-icon border-icon rounded-icon socicon-facebook" target="_blank"
                               href="https://www.facebook.com/UNIRvote/" title="Facebook"></a>

                            <a class="social-icon border-icon rounded-icon socicon-twitter" target="_blank"
                               href="https://twitter.com/UnirVote?s=08" title="Twitter"></a>

                            <a class="social-icon border-icon rounded-icon socicon-youtube" target="_blank"
                               href="https://www.youtube.com/channel/UC4xZ77411ZdUl_TY_gWXd5g" title="Youtube"></a>
                        </p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 col-md-pull-4 text-center">
                    <div class="widget widget_text">
                        <h3 class="widget-title">Nos Contacts</h3>
                        <ul class="list-unstyled greylinks">
                            <li class="media">
                                <i class="fa fa-phone highlight4 rightpadding_5"
                                   aria-hidden="true"></i> +228 90 99 21 77
                            </li>
                            <li class="media">
                                <i class="fa fa-pencil highlight4 rightpadding_5"
                                   aria-hidden="true"></i> <a
                                    href="mailto:info@unionpourlarepublique.tg">info@unionpourlarepublique.tg</a>
                            </li>
                            <li class="media">
                                <i class="fa fa-map-marker highlight4 rightpadding_5"
                                   aria-hidden="true"></i> Avenue des Evalas, Lomé- TOGO
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <section class="ds ms parallax page_copyright overlay_color section_padding_top_30 section_padding_bottom_30">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <p>Copyright &copy; 2020 Smart-UNIR | ITPLEX</p>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- eof #box_wrapper -->
</div>
<!-- eof #canvas -->
<!-- eof #canvas -->
<script src="{{ asset("home_page/js/compressed.js") }}"></script>
<script src="{{ asset("home_page/js/selectize.min.js") }}"></script>
<script src="{{ asset("home_page/js/main.js") }}"></script>
{{--<script src="js/switcher.js"></script>--}}

@include('flashy::message')
</body>
<!-- Mirrored from webdesign-finder.com/html/diversify/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 01 Dec 2019 21:15:23 GMT -->
</html>
