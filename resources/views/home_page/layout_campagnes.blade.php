<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<!-- Mirrored from webdesign-finder.com/html/diversify/services.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 01 Dec 2019 21:17:28 GMT -->
<head>
    <title>@yield('title') </title>
    <link rel="icon" href="{{ asset("home_page/images/logo_unir.jpg")}}">

    <meta charset="utf-8">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="stylesheet" href="{{ asset('home_page/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('home_page/css/animations.css') }}">
    <link rel="stylesheet" href="{{ asset('home_page/css/fonts.css') }}">
    <link rel="stylesheet" href="{{ asset('home_page/css/main.css') }}" class="color-switcher-link">
    <link rel="stylesheet" href="{{ asset('home_page/css/shop.css') }}" class="color-switcher-link">
    <script src="{{ asset('home_page/js/vendor/modernizr-2.6.2.min.js') }}"></script>
<!--[if lt IE 9]>
		<script src="{{ asset('home_page/js/vendor/html5shiv.min.js') }}"></script>
		<script src="{{ asset('home_page/js/vendor/respond.min.js') }}"></script>
		<script src="{{ asset('home_page/js/vendor/jquery-1.12.4.min.js') }}"></script>
	<![endif]-->
</head>

<body>
<!--[if lt IE 9]>
<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a
    href="http://browsehappy.com/" class="highlight">upgrade your browser</a> to improve your experience.
</div>
<![endif]-->
<div class="preloader">
    <div class="preloader_image"></div>
</div>
<!-- search modal -->
<div class="modal" tabindex="-1" role="dialog" aria-labelledby="search_modal" id="search_modal">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">
			<i class="rt-icon2-cross2"></i>
		</span>
    </button>
    <div class="widget widget_search">
        <form method="get" class="searchform search-form form-inline"
              action="http://webdesign-finder.com/html/diversify/">
            <div class="form-group bottommargin_0"><input type="text" value="" name="search" class="form-control"
                                                          placeholder="Search keyword" id="modal-search-input"></div>
            <button type="submit" class="theme_button no_bg_button">Search</button>
        </form>
    </div>
</div>
<!-- Unyson messages modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="messages_modal">
    <div class="fw-messages-wrap ls with_padding">
        <!-- Uncomment this UL with LI to show messages in modal popup to your user: -->
        <!--
    <ul class="list-unstyled">
        <li>Message To User</li>
    </ul>
    -->
    </div>
</div>
<!-- eof .modal -->
<!-- wrappers for visual page editor and boxed version of template -->
<div id="canvas">
    <div id="box_wrapper">
        <!-- template sections -->
        <header class="page_header header_white  toggler_xs_right columns_margin_0">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12 display_table">
                        <div class="header_left_logo display_table_cell">
                            <a href="{{ route("home.page") }}" class="logo logo_with_text">
                                <img src="{{ asset("home_page/images/logo_unir.jpg")}}" alt="">
                                <span class="logo_text">
                                    Smart-Unir
{{--                                    <small class="highlight5">Suivi Scolaire</small>--}}
                                </span>
                            </a>
                        </div>

                        <div class="header_mainmenu display_table_cell text-center">
                            <!-- main nav start -->
                            <nav class="mainmenu_wrapper">
                                <ul class="mainmenu nav sf-menu">
                                    <li class="active"><a href="{{ route('home.page')}}">Accueil</a></li>
                                    <li><a href="{{ route('home.page').'#plateforme'}}">Plateforme</a></li>
                                    <li><a href="{{ route('home.page').'#campagne'}}">Campagne 2020</a></li>
                                    <li><a href="{{ route('home.page').'#unir-mouvements'}}">UNIR & Mouvements</a></li>
                                    <li><a href="{{ route('home.page').'#gallery'}}">Galerie</a></li>
                                    <li><a href="{{ url()->current().'#subscribe'}}">Newsletter</a></li>
                                </ul>
                            </nav>
                            <!-- eof main nav -->
                            <!-- header toggler --><span class="toggle_menu"><span></span></span>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <section class="page_breadcrumbs ds background_cover section_padding_top_65 section_padding_bottom_65">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h2>@yield('h2_title')</h2>
                    </div>
                </div>
            </div>
        </section>

        @yield('content')

        <footer
            class="page_footer template_footer ds ms parallax overlay_color section_padding_top_110 section_padding_bottom_100 columns_padding_25">
            <div class="container">
                <div class="row">
                    @include('home_page.newsletter')
                    <div class="col-md-4 col-sm-6 col-xs-12 col-md-push-4 col-md-offset-2 text-center">
                        <div class="widget widget_text widget_about">
                            <div class="logo logo_with_text bottommargin_10"><img
                                    src="{{ asset("home_page/images/logo_unir.jpg")}}" alt="">
                            </div>
                            <p>UNIR est un Parti politique porté sur les fonts baptismaux
                                lors de son Assemblée générale constitutive du 14 avril 2012 à Atakpamé
                                et signifie « Union pour la République ».</p>
                            <p class="topmargin_25">
                                <a class="social-icon border-icon rounded-icon socicon-facebook" target="_blank"
                                   href="https://www.facebook.com/UNIRvote/" title="Facebook"></a>

                                <a class="social-icon border-icon rounded-icon socicon-twitter" target="_blank"
                                   href="https://twitter.com/UnirVote?s=08" title="Twitter"></a>

                                <a class="social-icon border-icon rounded-icon socicon-youtube" target="_blank"
                                   href=" https://www.youtube.com/channel/UC4xZ77411ZdUl_TY_gWXd5g" title="Youtube"></a>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 col-md-pull-4 text-center">
                        <div class="widget widget_text">
                            <h3 class="widget-title">Nos Contacts</h3>
                            <ul class="list-unstyled greylinks">
                                <li class="media"><i class="fa fa-phone highlight4 rightpadding_5"
                                                     aria-hidden="true"></i> +228 90 99 21 77
                                </li>
                                <li class="media"><i class="fa fa-pencil highlight4 rightpadding_5"
                                                     aria-hidden="true"></i> <a
                                        href="mailto:info@ops.tg">info@unionpourlarepublique.tg</a>
                                </li>
                                <li class="media"><i class="fa fa-map-marker highlight4 rightpadding_5"
                                                     aria-hidden="true"></i> Avenue des Evalas, Lomé- TOGO
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <section class="ds ms parallax page_copyright overlay_color section_padding_top_30 section_padding_bottom_30">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <p>&copy; Copyright 2018. All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- eof #box_wrapper -->
</div>
<!-- eof #canvas -->
<script src="{{ asset("home_page/js/compressed.js") }}"></script>
<script src="{{ asset("home_page/js/selectize.min.js") }}"></script>
<script src="{{ asset("home_page/js/main.js") }}"></script>
{{--<script src="js/switcher.js"></script>--}}
</body>


<!-- Mirrored from webdesign-finder.com/html/diversify/services.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 01 Dec 2019 21:17:28 GMT -->
</html>
