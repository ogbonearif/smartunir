<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

//home page routes
Route::get('/', 'HomePageController@index')->name('home.page');
Route::view('challenge/smart/unir', 'home_page/challenge_smart_unir')->name('challenge.smart.unir');
Route::view('programme/campagne', 'home_page/programme')->name('programme.campagne');
Route::view('croire/en/togo', 'home_page/croire')->name('croire');
Route::view('osons/davantage', 'home_page/osons_davantage')->name('osons');
Route::view('mot/president', 'home_page/mot_president')->name('mot.president');

Route::get('coming-soon/index', 'ComingSoonController@index')->name('coming_soon');

// Newsletter routes
Route::get('newsletter/store', 'Admin\NewsletterController@store')->name('store.newsletter');
Route::post('newsletter/store', 'Admin\NewsletterController@_store')->name('admin.store.newsletter');

Route::prefix('admin')->namespace('Admin')->name('admin.')->group(function () {
    Auth::routes();
    Route::get('/home', 'AdminHomeController@index')->name('home');
    Route::resource('buttons', 'ButtonController');

    Route::post('slide/position/update', 'SlideController@update_slide_position');
    Route::resource('slides', 'SlideController');

    Route::resource('newsletters', 'NewsletterController');
    Route::resource('newsletterMessages', 'NewsletterMessageController');

    Route::resource('parameters', 'ParameterController');
});
