<?php

namespace App\Repositories\Admin;

use App\Models\Parameter;
use App\Repositories\BaseRepository;

/**
 * Class ParameterRepository
 * @package App\Repositories\Admin
 * @version February 3, 2020, 9:13 am UTC
*/

class ParameterRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'label',
        'description',
        'value'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Parameter::class;
    }
}
