<?php

namespace App\Repositories\Admin;

use App\Models\NewsletterMessage;
use App\Repositories\BaseRepository;

/**
 * Class NewsletterMessageRepository
 * @package App\Repositories\Admin
 * @version January 21, 2020, 6:07 pm UTC
*/

class NewsletterMessageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'message'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return NewsletterMessage::class;
    }
}
