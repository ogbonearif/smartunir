<?php

namespace App\Repositories\Admin;

use App\Models\Button;
use App\Repositories\BaseRepository;

/**
 * Class ButtonRepository
 * @package App\Repositories\Admin
 * @version January 15, 2020, 6:12 pm UTC
*/

class ButtonRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'label',
        'link',
        'visibility'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Button::class;
    }
}
