<?php

namespace App\DataTables\Admin;

use App\Models\NewsletterMessage;
use Illuminate\Support\Str;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class NewsletterMessageDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        $dataTable->editColumn('message', function (NewsletterMessage $newsletterMessage){
            return Str::limit( $newsletterMessage->message, 100);
        })->rawColumns(['message', 'action']);

        return $dataTable->addColumn('action', 'admins.newsletter_messages.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\NewsletterMessage $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(NewsletterMessage $model)
    {
        return $model->newQuery()->orderByDesc('created_at');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    /*['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],*/
                ],
                'language' => [
                    'url' => asset('lang/' . config('app.locale') . '.json')
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'subject' => ['title' => __('Subject')],
            'message',
            'created_at' => ['title' => __('Send At')]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'newsletter_messagesdatatable_' . time();
    }
}
