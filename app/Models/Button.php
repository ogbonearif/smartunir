<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Button
 * @package App\Models
 * @version January 15, 2020, 6:12 pm UTC
 *
 * @property string label
 * @property string link
 * @property boolean visibility
 */
class Button extends Model
{

    public $table = 'buttons';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const COLORS = ['color_white' => 'Blanc', 'color3' => 'Jaune', 'color4' => 'Bleu', 'color6' => 'Noir'];

    public $fillable = [
        'label',
        'color',
        'link',
        'visibility'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'label' => 'string',
        'link' => 'string',
        'visibility' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'label' => ['required'],
        'link' => ['required', 'url'],
        'visibility' => 'required'
    ];

}
