<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class NewsletterMessage
 * @package App\Models
 * @version January 21, 2020, 6:07 pm UTC
 *
 * @property string title
 * @property string description
 */
class NewsletterMessage extends Model
{

    public $table = 'newsletter_messages';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'subject',
        'message'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'subject' => 'string',
        'title' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        //'title' => 'required',
        'message' => 'required'
    ];

}
