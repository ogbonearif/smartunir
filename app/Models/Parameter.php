<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Parameter
 * @package App\Models
 * @version February 3, 2020, 9:13 am UTC
 *
 * @property string label
 * @property string description
 * @property string value
 */
class Parameter extends Model
{

    public $table = 'parameters';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const COMING_SOON_DATE = 'COMING_SOON_DATE';

    const DEFAULTS = [
        self::COMING_SOON_DATE => ["COMING SOON DATE", '2020/02/02']
    ];

    public $fillable = [
        'label',
        'description',
        'value'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'label' => 'string',
        'description' => 'string',
        'value' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'label' => 'required',
        'description' => 'required',
        'value' => 'required'
    ];


}
