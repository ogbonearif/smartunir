<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Support\Facades\Auth;

/**
 * Class Slide
 * @package App\Models
 * @version January 16, 2020, 1:13 pm UTC
 *
 * @property string image
 * @property string title1
 * @property string title2
 * @property string button_label
 * @property string button_link
 * @property integer image_position
 */
class Slide extends Model
{

    public $table = 'slides';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
//    const MAX_IMAGE_SIZE = 8000000;
   /* const MAX_IMAGE_SIZE = 8000000;
    const IMAGE_WIDTH = 1920;
    const IMAGE_HEIGHT = 1270;*/


    public $fillable = [
        'image',
        'title1',
        'title2',
        'button_label',
        'button_link',
        'button_color',
        'slide_position'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'image' => 'string',
        'title1' => 'string',
        'title2' => 'string',
        'button_label' => 'string',
        'button_link' => 'string',
        'button_color' => 'string',
        'slide_position' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
       /* 'title1' => 'required',
        'title2' => 'required',
        'button_label' => 'required',*/

    ];

    public function getImageUrlAttribute()
    {
        return asset('storage/slides/'. $this->image);
    }
}
