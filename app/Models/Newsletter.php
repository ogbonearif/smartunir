<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Newsletter
 * @package App\Models
 * @version January 18, 2020, 12:46 pm UTC
 *
 * @property string email
 */
class Newsletter extends Model
{

    public $table = 'newsletters';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'email'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'email' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'Email' => ['required', 'string', 'email', 'max:255'],
    ];

}
