<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Slide;

class UpdateSlideRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = Slide::$rules;
        $rules += ['image' => ['file', 'dimensions:min_width=1920,min_height=1270', 'max:8000']];
        return $rules;
    }

    /**
     * Customizing The Error Messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'image.dimensions' => "L'image n'est pas conforme, elle doit avoir : (Largeur >= 1920 pixel, Hauteur >= 1270 pixel et 8Mo de taille maximale)."
        ];
    }
}
