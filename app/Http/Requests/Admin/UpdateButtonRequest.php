<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Button;
use Illuminate\Validation\Rule;

class UpdateButtonRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Button::$rules;
        array_push($rules['label'] , Rule::unique('buttons')->ignore(request()->button, 'id'));
        array_push($rules['link'] , Rule::unique('buttons')->ignore(request()->button, 'id'));

        return $rules;
    }
}
