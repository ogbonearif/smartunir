<?php

namespace App\Http\Controllers;

use App\Models\Button;
use App\Models\Slide;
use Illuminate\Http\Request;

class HomePageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('coming.soon');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $slides = Slide::orderBy('slide_position', 'ASC')->select('id', 'image', 'title1', 'title2', 'button_label',
            'button_link','button_color', 'slide_position')->get();

        $visible_btn = Button::where('visibility', 1)->first();

        $all_gallery_images = ['F01.jpg', 'F02.jpg', 'MFU.png', 'MJU.png'];

        return view('home_page.index', compact('visible_btn', 'slides', 'all_gallery_images'));
    }
}
