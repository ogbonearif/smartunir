<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\NewsletterDataTable;
use App\Http\Requests\Admin\CreateNewsletterRequest;
use App\Http\Requests\Admin\UpdateNewsletterRequest;
use App\Models\Newsletter;
use App\Repositories\Admin\NewsletterRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use MercurySeries\Flashy\Flashy;
use Response;
use App\Models\Admin;

class NewsletterController extends AppBaseController
{
    /** @var  NewsletterRepository */
    private $newsletterRepository;

    public function __construct(NewsletterRepository $newsletterRepo)
    {
        $this->middleware('auth:' . Admin::GUARD)->except('store');
        $this->newsletterRepository = $newsletterRepo;
    }

    /**
     * Display a listing of the Newsletter.
     *
     * @param NewsletterDataTable $newsletterDataTable
     * @return Response
     */
    public function index(NewsletterDataTable $newsletterDataTable)
    {
        return $newsletterDataTable->render('admins.newsletters.index');
    }

    /**
     * Show the form for creating a new Newsletter.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admins.newsletters.create');
    }

    /**
     * Store a newly created Newsletter in storage.
     *
     * @param CreateNewsletterRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateNewsletterRequest $request)
    {
        if (! Newsletter::where('email', '=', $request['Email'])->first()){
            Newsletter::create(['email' => $request['Email']]);
            Flashy::info(__("Thanks for subscribe to our newsletter"));

            return $request['coming-soon'] ? redirect()->route('coming_soon') :
                redirect()->route('home.page');
        }
        Flashy::info(__("Sorry! You have already subscribed"));

        return $request['coming-soon'] ? redirect()->route('coming_soon') : redirect()->route('home.page');
    }

    public function _store(Request $request)
    {
        $this->validate($request, [
            'email' => ['required', 'string', 'email', 'unique:newsletters', 'max:255'],
        ]);
        Newsletter::create(['email' => $request['email']]);
        Flash::success(__("Thanks for subscribe to our newsletter"));

        return redirect()->route('admin.newsletters.index');
    }

    /**
     * Display the specified Newsletter.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View|\Illuminate\Routing\Redirector
     */
    public function show($id)
    {
        $newsletter = $this->newsletterRepository->find($id);

        if (empty($newsletter)) {
            Flash::error('Newsletter not found');

            return redirect(route('admin.newsletters.index'));
        }

        return view('admins.newsletters.show')->with('newsletter', $newsletter);
    }

    /**
     * Show the form for editing the specified Newsletter.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View|\Illuminate\Routing\Redirector
     */
    public function edit($id)
    {
        $newsletter = $this->newsletterRepository->find($id);

        if (empty($newsletter)) {
            Flash::error('Newsletter not found');

            return redirect(route('admin.newsletters.index'));
        }

        return view('admins.newsletters.edit')->with('newsletter', $newsletter);
    }

    /**
     * Update the specified Newsletter in storage.
     *
     * @param  int              $id
     * @param UpdateNewsletterRequest $request
     *
     * @return \Illuminate\Routing\Redirector
     */
    public function update($id, UpdateNewsletterRequest $request)
    {
        $newsletter = $this->newsletterRepository->find($id);

        if (empty($newsletter)) {
            Flash::error('Newsletter not found');

            return redirect(route('admin.newsletters.index'));
        }

        $newsletter = $this->newsletterRepository->update($request->all(), $id);

        Flash::success('Newsletter updated successfully.');

        return redirect(route('admin.newsletters.index'));
    }

    /**
     * Remove the specified Newsletter from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $newsletter = $this->newsletterRepository->find($id);

        if (empty($newsletter)) {
            Flash::error('Newsletter not found');

            return redirect(route('admin.newsletters.index'));
        }

        $this->newsletterRepository->delete($id);

        Flash::success('Newsletter deleted successfully.');

        return redirect(route('admin.newsletters.index'));
    }
}
