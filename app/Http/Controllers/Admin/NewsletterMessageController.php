<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\NewsletterMessageDataTable;
use App\Http\Requests\Admin\CreateNewsletterMessageRequest;
use App\Http\Requests\Admin\UpdateNewsletterMessageRequest;
use App\Mail\NewsletterMailable;
use App\Models\Admin;
use App\Models\Newsletter;
use App\Repositories\Admin\NewsletterMessageRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Mail;
use Response;

class NewsletterMessageController extends AppBaseController
{
    /** @var  NewsletterMessageRepository */
    private $newsletterMessageRepository;

    public function __construct(NewsletterMessageRepository $newsletterMessageRepo)
    {
        $this->middleware('auth:' . Admin::GUARD);
        $this->newsletterMessageRepository = $newsletterMessageRepo;
    }

    /**
     * Display a listing of the NewsletterMessage.
     *
     * @param NewsletterMessageDataTable $newsletterMessageDataTable
     * @return Response
     */
    public function index(NewsletterMessageDataTable $newsletterMessageDataTable)
    {
        return $newsletterMessageDataTable->render('admins.newsletter_messages.index');
    }

    /**
     * Show the form for creating a new NewsletterMessage.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admins.newsletter_messages.create');
    }

    /**
     * Store a newly created NewsletterMessage in storage.
     *
     * @param CreateNewsletterMessageRequest $request
     *
     * @return \Illuminate\Routing\Redirector
     */
    public function store(CreateNewsletterMessageRequest $request)
    {
        $input = $request->all();
        $mailable = new NewsletterMailable($request['subject'], $request['message']);
        Mail::bcc(Newsletter::all('email'))->queue($mailable);

        $newsletterMessage = $this->newsletterMessageRepository->create($input);

        Flash::success(__('Message saved successfully.'));

        return redirect(route('admin.newsletterMessages.index'));
    }

    /**
     * Display the specified NewsletterMessage.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View|\Illuminate\Routing\Redirector
     */
    public function show($id)
    {
        $newsletterMessage = $this->newsletterMessageRepository->find($id);

        if (empty($newsletterMessage)) {
            Flash::error(__('Message not found'));

            return redirect(route('admin.newsletterMessages.index'));
        }
        return view('admins.newsletter_messages.show')->with('newsletterMessage', $newsletterMessage);
    }

    /**
     * Show the form for editing the specified NewsletterMessage.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View|\Illuminate\Routing\Redirector
     */
    /*public function edit($id)
    {
        $newsletterMessage = $this->newsletterMessageRepository->find($id);

        if (empty($newsletterMessage)) {
            Flash::error('Newsletter Message not found');

            return redirect(route('admin.newsletterMessages.index'));
        }
        return view('admins.newsletter_messages.edit')->with('newsletterMessage', $newsletterMessage);
    }*/

    /**
     * Update the specified NewsletterMessage in storage.
     *
     * @param  int              $id
     * @param UpdateNewsletterMessageRequest $request
     *
     * @return \Illuminate\Routing\Redirector
     */
    /*public function update($id, UpdateNewsletterMessageRequest $request)
    {
        $newsletterMessage = $this->newsletterMessageRepository->find($id);

        if (empty($newsletterMessage)) {
            Flash::error('Newsletter Message not found');

            return redirect(route('admin.newsletterMessages.index'));
        }
        $newsletterMessage = $this->newsletterMessageRepository->update($request->all(), $id);

        Flash::success('Newsletter Message updated successfully.');

        return redirect(route('admin.newsletterMessages.index'));
    }*/

    /**
     * Remove the specified NewsletterMessage from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $newsletterMessage = $this->newsletterMessageRepository->find($id);

        if (empty($newsletterMessage)) {
            Flash::error(__('Message not found'));

            return redirect(route('admin.newsletterMessages.index'));
        }
        $this->newsletterMessageRepository->delete($id);

        Flash::success(__('Message deleted succesfully.'));

        return redirect(route('admin.newsletterMessages.index'));
    }
}
