<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\ButtonDataTable;
use App\Http\Requests\Admin\CreateButtonRequest;
use App\Http\Requests\Admin\UpdateButtonRequest;
use App\Models\Admin;
use App\Models\Button;
use App\Repositories\Admin\ButtonRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ButtonController extends AppBaseController
{
    /** @var  ButtonRepository */
    private $buttonRepository;

    public function __construct(ButtonRepository $buttonRepo)
    {
        $this->middleware('auth:' . Admin::GUARD);
        $this->buttonRepository = $buttonRepo;
    }

    /**
     * Display a listing of the Button.
     *
     * @param ButtonDataTable $buttonDataTable
     * @return Response
     */
    public function index(ButtonDataTable $buttonDataTable)
    {
        return $buttonDataTable->render('admins.buttons.index');
    }

    /**
     * Show the form for creating a new Button.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admins.buttons.create');
    }

    /**
     * Store a newly created Button in storage.
     *
     * @param CreateButtonRequest $request
     *
     * @return \Illuminate\Routing\Redirector
     */
    public function store(CreateButtonRequest $request)
    {
        $input = $request->all();
        if($input['visibility']){
            $this->change_visible_btn();
        }
        if (!$input['color']){
           $input['color'] = 'color_white';
        }
        $button = $this->buttonRepository->create($input);

        Flash::success(__('Button saved successfully.'));

        return redirect(route('admin.buttons.index'));
    }

    /**
     * Display the specified Button.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View|\Illuminate\Routing\Redirector
     */
    public function show($id)
    {
        $button = $this->buttonRepository->find($id);

        if (empty($button)) {
            Flash::error(__('Button not found'));
            return redirect(route('admin.buttons.index'));
        }

        return view('admins.buttons.show')->with('button', $button);
    }

    /**
     * Show the form for editing the specified Button.
     *
     * @param  int $id
     *
     * @return\Illuminate\View\View|\Illuminate\Routing\Redirector
     */
    public function edit($id)
    {
        $button = $this->buttonRepository->find($id);

        if (empty($button)) {
            Flash::error(__('Button not found'));
            return redirect(route('admin.buttons.index'));
        }

        return view('admins.buttons.edit')->with('button', $button);
    }

    /**
     * Update the specified Button in storage.
     *
     * @param  int              $id
     * @param UpdateButtonRequest $request
     *
     * @return \Illuminate\Routing\Redirector
     */
    public function update($id, UpdateButtonRequest $request)
    {
        $button = $this->buttonRepository->find($id);

        if (empty($button)) {
            Flash::error(__('Button not found'));
            return redirect(route('admin.buttons.index'));
        }
        if($request->all()['visibility']){
            $this->change_visible_btn();
        }
        $button = $this->buttonRepository->update($request->all(), $id);

        Flash::success(__('Button updated successfully.'));

        return redirect(route('admin.buttons.index'));
    }

    /**
     * Remove the specified Button from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $button = $this->buttonRepository->find($id);

        if (empty($button)) {
            Flash::error(__('Button not found'));
            return redirect(route('admin.buttons.index'));
        }

        $this->buttonRepository->delete($id);

        Flash::success(__('Button deleted successfully.'));

        return redirect(route('admin.buttons.index'));
    }

    public function change_visible_btn()
    {
        $visible_btn = Button::where('visibility', 1)->first();
        if ($visible_btn){
            $visible_btn->visibility = false;
            $visible_btn->save();
        }
    }
}
