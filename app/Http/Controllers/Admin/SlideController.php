<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\SlideDataTable;
use App\Http\Requests\Admin\CreateSlideRequest;
use App\Http\Requests\Admin\UpdateSlideRequest;
use App\Models\Slide;
use App\Repositories\Admin\SlideRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Response;

class SlideController extends AppBaseController
{
    /** @var  SlideRepository */
    private $slideRepository;

    public function __construct(SlideRepository $slideRepo)
    {
        $this->middleware('auth:' . \App\Models\Admin::GUARD);

        $this->slideRepository = $slideRepo;
    }

    /**
     * Display a listing of the Slide.
     *
     * @param SlideDataTable $slideDataTable
     * @return Response
     */
    public function index(SlideDataTable $slideDataTable)
    {
        $slides = Slide::orderBy('slide_position', 'ASC')->select('id', 'image', 'title1', 'title2', 'button_label',
            'button_link', 'button_color', 'slide_position')->get();
        return $slideDataTable->render('admins.slides.index', compact('slides'));
    }

    public function update_slide_position(Request $request)
    {
        $slides = Slide::all();
        foreach ($slides as $slide){
            $slide->timestamps = false;
            $id = $slide->id;

            foreach ($request->positions as $position){
                if ($position['id'] == $id)
                    $slide->update(['slide_position' => $position['position']]);
            }
        }
    }
    /**
     * Show the form for creating a new Slide.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admins.slides.create');
    }

    /**
     * Store a newly created Slide in storage.
     *
     * @param CreateSlideRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CreateSlideRequest $request)
    {

        if (!$request['button_color']){
            $request['button_color'] = 'color4';
        }
        if ($request->hasFile('image')){
            $image = $request->file('image');
            if ($this->is_image_allowed($image)){
                $image_name = (String)Str::uuid() . '.' . $image->getClientOriginalExtension();
                $image->storeAs('public/slides/' , $image_name);

                $this->slideRepository->create([
                    'image' => $image_name,
                    'title1' => $request['title1'],
                    'title2' => $request['title2'],
                    'button_label' => $request['button_label'],
                    'button_link' => $request['button_link'],
                    'button_color' => $request['button_color'],
                    'slide_position' => Slide::all()->count() + 1,
                ]);
                Flash::success('Slide saved successfully.');
            }
            else
                Flash::success('Slide saved fail.');
        }
        return redirect(route('admin.slides.index'));
    }

    /**
     * Verify if the specified file type is image
     * @param UploadedFile $image
     * @return bool
     */
    public function is_image_allowed(UploadedFile $image)
    {
        $mime_type = $image->getClientMimeType();
        // list($width, $height) = getimagesize($image);
        //return Str::startsWith($mime_type, 'image') && $width == Slide::IMAGE_WIDTH
        //   && $height == Slide::IMAGE_HEIGHT && $image->getSize() <= Slide::MAX_IMAGE_SIZE;
        return Str::startsWith($mime_type, 'image/');
    }

    /**
     * Display the specified Slide.
     *
     * @param  int $id
     *
     * @return \Illuminate\Routing\Redirector|\Illuminate\View\View|Response
     */
    public function show($id)
    {
        $slide = $this->slideRepository->find($id);

        if (empty($slide)) {
            Flash::error('Slide not found');

            return redirect(route('admin.slides.index'));
        }

        return view('admins.slides.show')->with('slide', $slide);
    }

    /**
     * Show the form for editing the specified Slide.
     *
     * @param  int $id
     *
     * @return\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        $slide = $this->slideRepository->find($id);

        if (empty($slide)) {
            Flash::error('Slide not found');

            return redirect(route('admin.slides.index'));
        }
        return view('admins.slides.edit', compact('slide'));
    }

    /**
     * Update the specified Slide in storage.
     *
     * @param  int              $id
     * @param UpdateSlideRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, UpdateSlideRequest $request)
    {
        $slide = $this->slideRepository->find($id);
//        dd($slide);
        $image_name = "";
        if (empty($slide)) {
            Flash::error('Slide not found');

            return redirect(route('admin.slides.index'));
        }
        if ($request->hasFile('image')){
            $image = $request->file('image');
            if ($this->is_image_allowed($image)) {
                Storage::delete('public/slides/' .$slide->image);
                $image_name = (String)Str::uuid() . '.' . $image->getClientOriginalExtension();
                $image->storeAs('public/slides/', $image_name);
            }
            $this->slideRepository->update(
                [
                    'image' => $image_name,
                    'title1' => $request['title1'],
                    'title2' => $request['title2'],
                    'button_label' => $request['button_label'],
                    'button_link' => $request['button_link'],
                    'button_color' => $request['button_color'],
                    'slide_position' => $request['slide_position'],
                ], $id);

            Flash::success('Slide updated successfully.');
        }
        else{
            $this->slideRepository->update($request->all(), $id);
        }
        return redirect(route('admin.slides.index'));
    }

    /**
     * Remove the specified Slide from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */

    public function destroy($id)
    {
        $slide = $this->slideRepository->find($id);

        if (empty($slide)) {
            Flash::error('Slide not found');

            return redirect(route('admin.slides.index'));
        }

        $this->slideRepository->delete($id);
        Storage::delete('public/slides/' .$slide->image);

        Flash::success('Slide deleted successfully.');

        return redirect(route('admin.slides.index'));
    }
}
