<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\ParameterDataTable;
use App\Http\Requests\Admin\CreateParameterRequest;
use App\Http\Requests\Admin\UpdateParameterRequest;
use App\Models\Admin;
use App\Repositories\Admin\ParameterRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ParameterController extends AppBaseController
{
    /** @var  ParameterRepository */
    private $parameterRepository;

    public function __construct(ParameterRepository $parameterRepo)
    {
        $this->middleware('auth:' . Admin::GUARD);
        $this->parameterRepository = $parameterRepo;
    }

    /**
     * Display a listing of the Parameter.
     *
     * @param ParameterDataTable $parameterDataTable
     * @return Response
     */
    public function index(ParameterDataTable $parameterDataTable)
    {
        return $parameterDataTable->render('admins.parameters.index');
    }

    /**
     * Show the form for creating a new Parameter.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admins.parameters.create');
    }

    /**
     * Store a newly created Parameter in storage.
     *
     * @param CreateParameterRequest $request
     *
     * @return \Illuminate\Routing\Redirector
     */
    public function store(CreateParameterRequest $request)
    {
        $input = $request->all();

        $parameter = $this->parameterRepository->create($input);

        Flash::success(__('Parameter saved successfully.'));

        return redirect(route('admin.parameters.index'));
    }

    /**
     * Display the specified Parameter.
     *
     * @param  int $id
     *
     * @return \Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($id)
    {
        $parameter = $this->parameterRepository->find($id);

        if (empty($parameter)) {
            Flash::error(__('Parameter not found'));

            return redirect(route('admin.parameters.index'));
        }

        return view('admins.parameters.show')->with('parameter', $parameter);
    }

    /**
     * Show the form for editing the specified Parameter.
     *
     * @param  int $id
     *
     * @return \Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        $parameter = $this->parameterRepository->find($id);

        if (empty($parameter)) {
            Flash::error(__('Parameter not found'));

            return redirect(route('admin.parameters.index'));
        }

        return view('admins.parameters.edit')->with('parameter', $parameter);
    }

    /**
     * Update the specified Parameter in storage.
     *
     * @param  int              $id
     * @param UpdateParameterRequest $request
     *
     * @return \Illuminate\Routing\Redirector
     */
    public function update($id, UpdateParameterRequest $request)
    {
        $parameter = $this->parameterRepository->find($id);

        if (empty($parameter)) {
            Flash::error(__('Parameter not found'));

            return redirect(route('admin.parameters.index'));
        }

        $parameter = $this->parameterRepository->update($request->all(), $id);

        Flash::success(__('Parameter updated successfully.'));

        return redirect(route('admin.parameters.index'));
    }

    /**
     * Remove the specified Parameter from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $parameter = $this->parameterRepository->find($id);

        if (empty($parameter)) {
            Flash::error(__('Parameter not found'));

            return redirect(route('admin.parameters.index'));
        }

        $this->parameterRepository->delete($id);

        Flash::success(__('Parameter deleted successfully.'));

        return redirect(route('admin.parameters.index'));
    }
}
