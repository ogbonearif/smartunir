<?php

namespace App\Http\Controllers;

use App\Models\Parameter;

class ComingSoonController extends Controller
{
    public function construct()
    {

    }

    public function index()
    {
        /* Get the coming soon date from the database */
        $coming_soon_date = Parameter::whereLabel(Parameter::COMING_SOON_DATE)->first()->value;

        /* Get the remaining date between the current date and the coming soon date */
        $remaining_days = date_create(date("Y/m/d"))->diff(date_create($coming_soon_date))->days;

        return view("comingSoon.index", compact('remaining_days'));
    }
}
