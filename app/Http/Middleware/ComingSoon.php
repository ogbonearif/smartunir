<?php

namespace App\Http\Middleware;

use App\Models\Parameter;
use Closure;

class ComingSoon
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /* Get the coming soon date from the database */
        $coming_soon_date = date_create(Parameter::whereLabel(Parameter::COMING_SOON_DATE)->first()->value);

        if (date_create(date("Y/m/d")) < $coming_soon_date){
            return redirect(route("coming_soon"));
        }

        return $next($request);
    }
}
