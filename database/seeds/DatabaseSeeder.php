<?php

use App\Models\Admin;
use App\Models\Parameter;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (Admin::whereEmail("admin@unir.tg")->doesntExist()) {
            Admin::create([
                'full_name' => 'Admin Unir',
                'email' => 'admin@unir.tg',
                'phone_number' => '92931341',
                'password' => Hash::make('password')
            ]);
        }

        foreach (Parameter::DEFAULTS as $key => $value) {
            if (Parameter::whereLabel($key)->doesntExist()) {
                Parameter::create([
                    'label' => $key,
                    'description' => $value[0],
                    'value' => $value[1],
                ]);
            }
        }
    }
}
