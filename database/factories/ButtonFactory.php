<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Button;
use Faker\Generator as Faker;

$factory->define(Button::class, function (Faker $faker) {

    return [
        'label' => $faker->word,
        'link' => $faker->word,
        'visibility' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
