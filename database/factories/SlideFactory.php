<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Slide;
use Faker\Generator as Faker;

$factory->define(Slide::class, function (Faker $faker) {

    return [
        'image' => $faker->word,
        'title1' => $faker->word,
        'title2' => $faker->word,
        'button_label' => $faker->word,
        'button_link' => $faker->word,
        'image_position' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
